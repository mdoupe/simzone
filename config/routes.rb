Rails.application.routes.draw do

  root to: "index#home"

  resources :user, only: [ :new, :create, :edit, :update ]

  get "signin", to: "sessions#new", as: "new_session"
  post "signin", to: "sessions#create", as: "create_session"
  delete "signout", to: "sessions#destroy", as: "destroy_session"

  get "horses/mine", to: "horses#mine", as: "my_horses"
  get "horses/:id/transfer", to: "horses#transfer", as: "transfer_horses"
  post "horses/:id/transfer", to: "horses#give", as: "give_horses"
  get "horses/:id/stable", to: "horses#stable", as: "stable_horses"
  post "horses/:id/stable", to: "horses#house", as: "house_horses"
  resources :horses, except: [:destroy]

  # get "stables/mine", to: "stables#mine", as: "my_stables"
  resources :stables do
    collection do
      get "mine", as: "my"
    end
  end

  get "shows/active", to: "shows#active", as: "active_shows"
  get "shows/mine", to: "shows#mine", as: "my_shows"
  resources :shows do
    resources :classes, shallow: true, controller: 'klasses', except: [:index] do
      member do
        post 'move_up'
        post 'move_down'
      end
      resources :requirements, shallow: true, except: [:index, :show]
      resources :entries, shallow: true, only: [:new, :create, :destroy]
    end
  end

end
