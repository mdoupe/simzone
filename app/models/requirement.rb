class Requirement < ActiveRecord::Base
  belongs_to :klass

  validates :klass_id, presence: true
  validates :quality, presence: true
  validates :inclusive, presence: true
  validates :values, presence: true

  def qualify?(horse)
    self.values.split(",").include?(horse[self.quality].to_s.downcase)
  end

  def owned_by?(user)
    self.klass.show.user == user
  end

  def destructable?
    self.klass.open? && !self.klass.entered?
  end
end