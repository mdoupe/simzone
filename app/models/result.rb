class Result < ActiveRecord::Base
  belongs_to :horse
  belongs_to :klass

  validates :klass, presence: true
  validates :horse_id, presence: true, uniqueness: {scope: :klass, message: "can only be entered once per class"}
  validates :finish, presence: true, uniqueness: {scope: :klass, message: "can only be ranked once per class"}
end
  