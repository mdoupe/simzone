class Klass < ActiveRecord::Base
  belongs_to :show
  has_many :requirements
  has_many :entries
  has_many :horses, through: :entries
  has_many :results
  #TODO: Add test for results

  acts_as_list scope: :show

  validates :show_id, presence: true
  validates :name, {presence: true, length: {within: (5..30)}}
  validates :entry_limit, {presence: true, numericality: true}
  validates :judged, exclusion: {in: [nil]}

  def destructable?
    !self.judged? && self.horses.count == 0
  end

  def owned_by?(user)
    user == show.user
  end

  def eligible?(horse)
    if horse.class.name == "Horse" && !self.horses.include?(horse)
      eligible = true
      self.requirements.each do |r|
        unless r.qualify?(horse) == true
          eligible = false
        end
      end
      return eligible
    else
      false
    end
  end

  def open?
    self.show.open? && !self.full?
  end

  def full?
    self.horses.count == self.entry_limit
  end

  def entered?
    self.horses.count != 0
  end

  def enter(horse)
    if self.open? && self.eligible?(horse)
      self.horses << horse
      true
    else
      false
    end
  end

  def judge
    if self.entries && self.entries.count != 0
      self.method(self.show.generator.to_sym).call
      self.entries.each do |e|
        e.delete
      end
      self.judged = true
      self.save
    else
      false
    end
  end

  def standard
    entries = self.entries.map { |i| i }
    entries.shuffle!
    i = 1
    entries.each do |e|
      Result.new({klass: self, horse: e.horse, finish: i}).save
      i += 1
    end
  end
end
