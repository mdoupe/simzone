class OffsiteResult < ActiveRecord::Base
  belongs_to :horse

  validates :date, presence: true
  validates :show, presence: true
  validates :klass, presence: true
  validates :finish, presence: true
end
