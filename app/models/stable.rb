class Stable < ActiveRecord::Base
  include Ownable

  belongs_to :user
  has_many :horses
  has_many :shows

  validates :name, {presence: true, length: {within: (4..50)}}
  validates :user_id, presence: true
end
