module Ownable
  extend ActiveSupport::Concern

  included do
    scope :owned_by, ->(user_id) {where("user_id = ?", user_id)}
  end

  def owned_by?(owner)
    owner == self.user
  end

  def transfer(new_owner)
    unless new_owner.changed?
      self.user = new_owner
      true
    else
      false
    end
  end

  def transfer!(new_owner)
    transfer(new_owner)
    save!
  end

end