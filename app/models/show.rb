class Show < ActiveRecord::Base

  include Ownable

  belongs_to :user
  belongs_to :stable
  has_many :klasses, -> {order(position: :asc)}, dependent: :destroy
  has_many :results, through: :klasses
  #TODO: add test for this

  after_create :create_job

  enum generator: %w(test standard)

  validates :user_id, presence: true
  validates :stable_id, presence: true
  validates :name, {presence: true, length: {within: (4..30)}}
  validates :held_at, {presence: true, date: true}
  validates :deadline, {presence: true, numericality: true}
  validates :generator, presence: true

  scope :active, -> { where("held_at > ?", DateTime.now) }

  def open?
    DateTime.now < held_at - deadline.seconds
  end

  def active?
    DateTime.now < held_at
  end

  def destructable?
    dest = active?
    if dest
      klasses.each do |klass|
        unless klass.destructable?
          dest = false
        end
      end
    end
    return dest
  end

  def conduct
    if self.results.count == 0
      klasses.each do |k|
        k.judge unless k.judged?
      end
    else
      false
    end
  end

  def create_job
    ShowsJob.set(wait_until: self.held_at).perform_later(self.id)
  end
end
