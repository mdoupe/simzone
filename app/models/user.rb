class User < ActiveRecord::Base
  has_secure_password

  has_many :horses
  has_many :stables
  has_many :shows

  validates :name, {presence: true, uniqueness: true, length: { within: (4..15)}}
  validates :email, {presence: true, uniqueness: true, email: true}
  validates :password, {presence: true, length: {within: (4..15)}}
  validates :password_confirmation, {presence: true, length: {within: (4..15)}}
end
