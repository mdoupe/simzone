class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new

    can :read, :all
    can :manage, :all do |object|
        object.owned_by?(user)
    end

  end
end
