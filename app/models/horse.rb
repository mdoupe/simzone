class Horse < ActiveRecord::Base
  include Ownable

  belongs_to :user
  belongs_to :stable
  has_many :entries
  has_many :klasses, through: :entries

  enum gender: ["Mare", "Stallion", "Gelding"]

  validates :user_id, presence: true
  validates :name, presence: true
  validates :year,{ presence: true, numericality: true}
  validates :color, presence: true
  validates :breed, presence: true
  validates :gender, presence: true
  validates :stable_id, presence: true

  def house!(new)
    self.stable = new
    self.save
  end

end
