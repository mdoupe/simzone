 class KlassesController < ApplicationController

  before_filter :verify_login
  before_filter :get_klass, only: [:show, :edit, :update, :destroy, :move_up, :move_down]
  before_filter :get_show, only: [:new, :create]

  def show
  end

  def new
    if can? :edit, @show
      @klass = Klass.new.decorate
    else
      redirect_to show_url(@show), notice: owner_notice("show","add classes to")
    end
  end

  def create
    if can? :edit, @show
      @klass = Klass.new(klass_params)
      @klass.show = @show
      @klass.judged = false
      @klass.save
      redirect_to show_url(@show), notice: "Successfully created #{@klass.name}."
    else
      redirect_to show_url(@show), notice: owner_notice("show","add classes to")
    end
  end

  def edit
    unless can? :edit, @klass.show
      redirect_to show_url(@klass.show), notice: owner_notice("show","edit classes for")
    end
  end

  def update
    if can? :edit, @klass.show
      if @klass.update_attributes(klass_params)
        redirect_to show_url(@klass.show), notice: "Successfully updated #{@klass.name}."
      else
        render :edit
      end
    else
      redirect_to show_url(@klass.show), notice: owner_notice("show","edit classes for")
    end
  end

  def destroy
    unless can? :destroy, @klass
      redirect_to class_url(@klass)
    else
      if @klass.destructable?
        @klass.delete
        redirect_to show_url(@klass.show), notice: "Successfully deleted class."
      else
        redirect_to class_url(@klass), notice: "Could not delete class."
      end
    end
  end

  def move_up
    if can? :edit, @klass
      @klass.move_higher
      @notice = "Successfully reordered classes."
    else
      @notice = owner_notice("class","reorder")
    end
    redirect_to show_url(@klass.show), notice: @notice
  end

  def move_down
    if can? :edit, @klass
      @klass.move_lower
      @notice = "Successfully reordered classes."
    else
      @notice = owner_notice("class","reorder")
    end
    redirect_to show_url(@klass.show), notice: @notice
  end

  private
  def get_show
    @show = Show.find(params[:show_id]).decorate
  end

  def get_klass
    @klass = Klass.find(params[:id]).decorate
  end

  def klass_params
    params.require(:klass).permit(:name,:description,:discipline,:level,:entry_limit,:show,:show_id)
  end
end
