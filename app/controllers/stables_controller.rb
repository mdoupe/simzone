class StablesController < ApplicationController

  before_action :verify_login, except: [:index, :show]
  before_action :set_stable, only: [:show, :edit, :update, :destroy]

  def index
    @stables = Stable.all.decorate
  end

  def show
  end

  def new
    @stable = Stable.new
  end

  def edit
    unless can? :edit, @stable
      redirect_to stable_url(@stable), notice: owner_notice("stable")
    end
  end

  def create
    @stable = Stable.new(stable_params)
    @stable.user = current_user
    if @stable.save
      redirect_to stable_url(@stable), notice: "Successfully created #{@stable.name}."
    else
      render :new
    end
  end

  def update
    unless can? :update, @stable
      redirect_to stable_url(@stable), notice: owner_notice("stable")
    else
      if @stable.update_attributes(stable_params)
        redirect_to stable_url(@stable), notice: "Successfully updated #{@stable.name}."
      else
        render :edit
      end
    end
  end

  def destroy
    unless can? :delete, @stable
      redirect_to stable_url(@stable), notice: owner_notice("stable", "delete")
    else
      @stable.destroy
      redirect_to my_stables_url
    end
  end

  def mine
    @stables = Stable.where(user_id: current_user.id).decorate
  end

  private
    def set_stable
      @stable = Stable.find(params[:id]).decorate
    end

    def stable_params
      params.require(:stable).permit(:name, :description, :user, :user_id)
    end
end
