class EntriesController < ApplicationController

  before_filter :verify_login
  before_filter :get_klass_and_make_entry, only: [:new, :create]
  before_filter :get_horse, only: [:create]
  before_filter :get_entry, only: [:destroy]

  def new
  end

  def create
    if can?(:edit, @horse)
      if @klass.enter(@horse)
        redirect_to class_url(@klass), notice: "Successfully entered #{@horse.name} in #{@klass.name}."
      else
        redirect_to new_class_entry_path(klass_id: @klass), notice: "#{@horse.name} is not eligible for #{@klass.name}."
      end
    else
      render :new
    end
  end

  def destroy
    if can?(:edit, @entry.horse)
      @entry.delete
      redirect_to class_url(@entry.klass), notice: "Removed #{@entry.horse.name} from #{@entry.klass.name}."
    else
      redirect_to class_url(@entry.klass), notice: owner_notice("enter","horse")
    end
  end

  private
  def get_klass_and_make_entry
    @entry = Entry.new
    @klass = Klass.find(params[:class_id])
  end

  def get_horse
    @horse = Horse.find(params[:entry][:horse_id])
  end

  def get_entry
    @entry = Entry.find(params[:id])
  end
end