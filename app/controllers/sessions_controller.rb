class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by_name(params[:name])
    if @user.authenticate(params[:password])
      session[:user_id] = @user.id
      redirect_to root_url, notice: "Successfully signed in! Welcome back, #{@user.name}."
    else
      redirect_to new_session_url, notice: "Unable to sign in. Username or password is incorrect."
    end
  end

  def destroy
    reset_session
    redirect_to root_url
  end
end
