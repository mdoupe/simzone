class ShowsController < ApplicationController

  before_filter :verify_login
  before_filter :get_show, only: [:show, :edit, :update, :destroy]

  def index
    @shows = Show.all.decorate
  end

  def show

  end

  def new
    @show = Show.new.decorate
  end

  def create
    @show = Show.new(show_params)
    @show.user = current_user
    if @show.save
      redirect_to show_url(@show), notice: "Successfully created #{@show.name}."
    else
      render :new
    end
  end

  def edit
    unless can? :edit, @show
      redirect_to show_url(@show), notice: owner_notice("show")
    end
  end

  def update
    unless can? :update, @show
      redirect_to @show, notice: owner_notice("show")
    else
      if @show.update_attributes(show_params)
        redirect_to @show, notice: "Successfully updated #{@show.name}."
      else
        render :edit
      end
    end
  end

  def destroy
    if can? :destroy, @show
      if @show.destructable?
        @show.delete
        redirect_to shows_url, notice: "Successfully deleted show."
      else
        redirect_to @show, notice: "That show may not be destroyed."
      end
    else
      redirect_to @show, notice: owner_notice("show","delete")
    end
  end

  def mine
    @shows = Show.where(user_id: current_user.id)
  end

  def active
    @shows = Show.active
  end

  private
  def get_show
    @show = Show.find(params[:id]).decorate
  end

  def show_params
    params.require(:show).permit(:name,:description,:deadline,:held_at,:generator,:user_id,:user,:stable_id,:stable)
  end
end
