class RequirementsController < ApplicationController

  before_filter :verify_login
  before_filter :get_klass, only: [:new, :create]
  before_filter :get_requirement, only: [:edit, :update, :destroy]

  def new
    if can?(:edit, @klass) && !@klass.entered?
      @requirement = Requirement.new.decorate
    else
      redirect_to class_url(@klass)
    end
  end

  def create
    if can?(:edit, @klass) && !@klass.entered?
      @requirement = Requirement.new(requirement_params)
      @requirement.klass = @klass
      if @requirement.save
        redirect_to class_url(@klass), notice: "Successfully created requirement."
      else
        render :new
      end
    else
      redirect_to class_url(@klass), notice: "Unable to create requirement."
    end
  end

  def edit
    unless can?(:edit, @requirement) && !@requirement.klass.entered?
      redirect_to class_url(@requirement.klass)
    end
  end

  def update
    if can?(:edit, @requirement) && !@requirement.klass.entered?
      if @requirement.update_attributes(requirement_params)
        redirect_to class_url(@requirement.klass), notice: "Successfully updated requirement."
      else
        render :edit
      end
    else
      redirect_to class_url(@requirement.klass), notice: owner_notice("class")
    end
  end

  def destroy
    if can? :destroy, @requirement
      if @requirement.destructable?
        @requirement.delete
        notice = "Successfully deleted requirement."
      else
        notice = "Requirement cannot be deleted."
      end
    else
      notice = owner_notice("requirement", "delete")
    end
    redirect_to class_url(@requirement.klass), notice: notice
  end

  private
  def get_klass
    @klass = Klass.find(params[:class_id])
  end

  def get_requirement
    @requirement = Requirement.find(params[:id]).decorate
  end

  def requirement_params
    params.require(:requirement).permit(:klass,:klass_id,:quality,:inclusive,:values)
  end
end
