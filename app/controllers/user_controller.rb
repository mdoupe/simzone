class UserController < ApplicationController

  before_action :authorize, only: [:edit, :update]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to root_url, notice: "Successfully signed up!"
    else
      render :new, user: @user
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      redirect_to root_url, notice: "Successfully updated user information!"
    else
      render :edit
    end
  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def authorize
    @user = User.find(params[:id])
    if current_user.nil?
      send_to_login
    else
      unless @user && @user == current_user
        redirect_to edit_user_url(current_user.id)
      end
    end
  end
end
