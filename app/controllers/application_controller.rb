class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_user
    if session[:user_id]
      User.find(session[:user_id])
    else
      nil
    end
  end

  def send_to_login
    redirect_to new_session_url, notice: "You must sign in before performing that action."
  end

  def user_signed_in?
    !current_user.nil?
  end

  def verify_login
    unless user_signed_in?
      send_to_login
    end
  end

  def owner_notice(object, action = "edit")
    if object.class.name == "String"
      "Only the owner may #{action} #{aan(object)}."
    end
  end

  def aan(object)
    if object.class.name == "String"
      ["a","e","i","o","u"].include?(object[0]) ? "an #{object}" : "a #{object}"
    end
  end

  helper_method :current_user, :user_signed_in?
end
