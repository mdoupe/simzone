class HorsesController < ApplicationController
  include ApplicationHelper

  before_action :verify_login, except: [:show, :index]
  before_action :get_horse, only: [:edit, :update, :transfer, :give, :show, :stable, :house]

  def new
    @horse = Horse.new.decorate
    @stables = Stable.where(user_id: current_user.id).map { |stable| [stable.name, stable.id]}
  end

  def create
    @horse = Horse.new(horse_params)
    @horse.user_id = session[:user_id]
    if @horse.save
      redirect_to horse_url(@horse), notice: "Horse successfully created."
    else
      render :new
    end
  end

  def edit
    unless can? :edit, @horse
      redirect_to horse_url(@horse), notice: owner_notice("horse")
    end
  end

  def update
    if can? :update, @horse
      if @horse.update_attributes(horse_params)
        redirect_to horse_url(@horse), notice: "Successfully updated #{@horse.name}."
      else
        render :edit
      end
    else
      redirect_to horse_url(@horse), notice: owner_notice("horse")
    end
  end

  def show
  end

  def index
    @horses = Horse.all.decorate
  end

  def mine
    @horses = Horse.owned_by(current_user.id).decorate
  end

  def transfer
    unless can? :transfer, @horse
      redirect_to horse_url(@horse), notice: owner_notice("horse", "transfer")
    end
    @users = User.all.map { |u| [u.name, u.id] }
  end

  def give
    if can? :give,  @horse
      @new = User.find(params[:horse][:user_id])
      @horse.transfer(@new)
      @horse.save
      @notice = "Successfully transferred #{@horse.name} to #{@new.name}."
    else
      @notice = owner_notice("horse")
    end
    redirect_to horse_url(@horse), notice: @notice
  end

  def stable
    unless can? :edit, @horse
      redirect_to horses_url(@horse), notice: owner_notice("horse","restable")
    else
    end
  end

  def house
    unless can? :edit, @horse
      redirect_to horse_url(@horse), notice: owner_notice("horse","restable")
    else
      unless params[:horse][:stable_id] == nil
        @stable = Stable.find(params[:horse][:stable_id])
        @horse.house!(@stable)
        redirect_to horse_url(@horse), notice: "Successfully stabled #{@horse.name} at #{@stable.name}."
      else
        render :stable
      end
    end
  end

  private
  def horse_params
    params.require(:horse).permit(:name,:year,:color,:breed,:gender,:user_id,:stable_id)
  end

  def get_horse
    @horse = Horse.find(params[:id]).decorate
  end
end
