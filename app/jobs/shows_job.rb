class ShowsJob < ActiveJob::Base
  queue_as :default

  def perform(show_id)
    if Show.exists?(id: show_id)
      Show.find(show_id).conduct
    end
  end
end
