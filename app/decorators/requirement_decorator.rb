class RequirementDecorator < Draper::Decorator
  delegate_all

  def brief
    vals = values.split(",")
    vals.each_with_index do |v,i|
      vals[i] = v.slice(0,1).upcase + v.slice(1..-1)
    end
    vals = vals.join(", ")
    "Restricted to #{quality}s: #{vals}"
  end

end