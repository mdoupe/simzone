class KlassDecorator < Draper::Decorator
  delegate_all

  decorates_association :requirement

  def spots
    entry_limit - entries.count
  end
end
