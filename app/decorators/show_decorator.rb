class ShowDecorator < Draper::Decorator
  delegate_all

  decorates_association :klasses

  def link
    h.link_to(object.name, h.show_url(object))
  end
end
