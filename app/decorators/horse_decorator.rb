class HorseDecorator < Draper::Decorator
  delegate_all

  def age
    Date.today.strftime("%Y").to_i - object.year
  end

  def info
    "#{object.year} #{object.color} #{object.breed} #{object.gender}"
  end

  def owner
    object.user.name
  end

  def link
    h.link_to(object.name, h.horse_url(object))
  end

end
