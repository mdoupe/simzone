class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.references :klass
      t.references :horse
      t.integer :finish
      t.timestamps null: false
    end
  end
end
