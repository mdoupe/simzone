class CreateRequirements < ActiveRecord::Migration
  def change
    create_table :requirements do |t|
      t.references :klass, index: true, foreign_key: true
      t.string :quality
      t.boolean :inclusive
      t.text :values
      t.timestamps null: false
    end
  end
end
