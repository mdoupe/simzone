class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.references :klass
      t.references :horse
    end
  end
end
