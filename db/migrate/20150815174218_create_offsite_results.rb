class CreateOffsiteResults < ActiveRecord::Migration
  def change
    create_table :offsite_results do |t|
      t.references :horse, index: true, foreign_key: true
      t.date :date
      t.string :show
      t.string :klass
      t.string :finish
      t.string :url

      t.timestamps null: false
    end
  end
end
