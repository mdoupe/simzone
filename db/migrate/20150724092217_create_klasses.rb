class CreateKlasses < ActiveRecord::Migration
  def change
    create_table :klasses do |t|
      t.references :show, index: true, foreign_key: true
      t.string :name
      t.string :discipline
      t.string :level
      t.integer :entry_limit
      t.integer :position
      t.boolean :judged

      t.timestamps null: false
    end
  end
end
