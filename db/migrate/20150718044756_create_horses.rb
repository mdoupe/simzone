class CreateHorses < ActiveRecord::Migration
  def change
    create_table :horses do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.integer :year
      t.string :color
      t.string :breed
      t.integer :gender
      t.timestamps null: false
    end
  end
end
