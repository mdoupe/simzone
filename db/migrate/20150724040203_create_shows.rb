class CreateShows < ActiveRecord::Migration
  def change
    create_table :shows do |t|
      t.references :user, index: true, foreign_key: true
      t.references :stable, index: true, foreign_key: true
      t.string :name
      t.text :description
      t.datetime :held_at
      t.integer :deadline
      t.integer :generator
      t.timestamps null: false
    end
  end
end
