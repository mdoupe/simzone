class DropJoinTableHorsesKlasses < ActiveRecord::Migration
  def up
    drop_table :horses_klasses
  end

  def down
    create_join_table :klasses, :horses do |t|
      t.index [:klass_id, :horse_id]
      t.index [:horse_id, :klass_id]
    end
  end
end
