# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150815174218) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "entries", force: :cascade do |t|
    t.integer "klass_id"
    t.integer "horse_id"
  end

  create_table "horses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.integer  "year"
    t.string   "color"
    t.string   "breed"
    t.integer  "gender"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "stable_id"
  end

  add_index "horses", ["stable_id"], name: "index_horses_on_stable_id", using: :btree
  add_index "horses", ["user_id"], name: "index_horses_on_user_id", using: :btree

  create_table "klasses", force: :cascade do |t|
    t.integer  "show_id"
    t.string   "name"
    t.string   "discipline"
    t.string   "level"
    t.integer  "entry_limit"
    t.integer  "position"
    t.boolean  "judged"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "klasses", ["show_id"], name: "index_klasses_on_show_id", using: :btree

  create_table "offsite_results", force: :cascade do |t|
    t.integer  "horse_id"
    t.date     "date"
    t.string   "show"
    t.string   "klass"
    t.string   "finish"
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "offsite_results", ["horse_id"], name: "index_offsite_results_on_horse_id", using: :btree

  create_table "requirements", force: :cascade do |t|
    t.integer  "klass_id"
    t.string   "quality"
    t.boolean  "inclusive"
    t.text     "values"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "requirements", ["klass_id"], name: "index_requirements_on_klass_id", using: :btree

  create_table "results", force: :cascade do |t|
    t.integer  "klass_id"
    t.integer  "horse_id"
    t.integer  "finish"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shows", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "stable_id"
    t.string   "name"
    t.text     "description"
    t.datetime "held_at"
    t.integer  "deadline"
    t.integer  "generator"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "shows", ["stable_id"], name: "index_shows_on_stable_id", using: :btree
  add_index "shows", ["user_id"], name: "index_shows_on_user_id", using: :btree

  create_table "stables", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "stables", ["user_id"], name: "index_stables_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_foreign_key "horses", "stables"
  add_foreign_key "horses", "users"
  add_foreign_key "klasses", "shows"
  add_foreign_key "offsite_results", "horses"
  add_foreign_key "requirements", "klasses"
  add_foreign_key "shows", "stables"
  add_foreign_key "shows", "users"
  add_foreign_key "stables", "users"
end
