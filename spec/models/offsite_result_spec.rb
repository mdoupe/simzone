require 'rails_helper'

RSpec.describe OffsiteResult, type: :model do
  describe 'validates' do
    it 'the record' do
      expect(subject).to validate_presence_of :date
      expect(subject).to validate_presence_of :show
      expect(subject).to validate_presence_of :klass
      expect(subject).to validate_presence_of :finish
      expect(subject).to belong_to :horse
    end
  end

end
