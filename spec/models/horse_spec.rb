require 'rails_helper'

RSpec.describe Horse, type: :model do

  it_behaves_like "ownable"

  describe 'validates' do
    it 'presence of user_id' do
      expect(subject).to validate_presence_of :user_id
    end
    it 'presence of name' do
      expect(subject).to validate_presence_of :name
    end
    it 'presence of year' do
      expect(subject).to validate_presence_of :year
    end
    it 'presence of color' do
      expect(subject).to validate_presence_of :color
    end
    it 'presence of breed' do
      expect(subject).to validate_presence_of :breed
    end
    it 'presence of gender' do
      expect(subject).to validate_presence_of :gender
    end
    it 'numericality of year' do
      expect(subject).to validate_numericality_of :year
    end
    it 'presence of stable_id' do
      expect(subject).to validate_presence_of :stable_id
    end
  end

  describe 'associations' do
    it 'belongs to user' do
      expect(subject).to belong_to(:user)
    end

    it 'belongs to stable' do
      expect(subject).to belong_to(:stable)
    end

    it 'has many entries' do
      expect(subject).to have_many :entries
    end

    it 'has many klasses through entries' do
      expect(subject).to have_many(:klasses).through(:entries)
    end
  end

  describe 'attributes' do
    it 'defines enumerated list of genders' do
      expect(subject).to define_enum_for(:gender).with(["Mare","Stallion","Gelding"])
    end
  end

  describe 'methods' do
    describe '.house!' do
       it 'changes the horses stable' do
         @horse = create(:horse)
         @new = create(:stable)
         @horse.house!(@new)
         @horse.reload
         expect(@horse.stable).to eq(@new)
       end
     end
  end
end
