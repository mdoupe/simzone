require 'rails_helper'

RSpec.describe User, type: :model do

  describe 'validates' do

    it 'presence of name' do
      expect(subject).to validate_presence_of :name
    end

    it 'presence of email' do
      expect(subject).to validate_presence_of :email
    end

    it 'presence of password' do
      expect(subject).to validate_presence_of :password
    end

    it 'presence of password_confirmation' do
      expect(subject).to validate_presence_of :password_confirmation
    end

    it 'uniqueness of name' do
      expect(subject).to validate_uniqueness_of :name
    end

    it 'uniqueness of email' do
      expect(subject).to validate_uniqueness_of :email
    end

    it 'length of name' do
      expect(subject).to ensure_length_of(:name).is_at_least(4).is_at_most(15)
    end

    it 'length of password' do
      expect(subject).to ensure_length_of(:password).is_at_least(4).is_at_most(15)
    end

    it 'length of password_confirmation' do
      expect(subject).to ensure_length_of(:password_confirmation).is_at_least(4).is_at_most(15)
    end

    it 'confirmation of password' do
      expect(subject).to validate_confirmation_of(:password)
    end

    it 'email format' do
      expect(subject).to allow_value("test@example.com").for(:email)
      expect(subject).to_not allow_value("test").for(:email)
    end
  end

  describe 'associations' do

    it 'has many horses' do
      expect(subject).to have_many :horses  
    end

    it 'has many stables' do
      expect(subject).to have_many :stables
    end

    it 'has many shows' do
      expect(subject).to have_many :shows
    end
  end

  describe 'authentication' do
    it 'with secure password' do
      expect(subject).to have_secure_password
    end
  end

  describe 'abilities' do
    subject(:ability){ Ability.new(user) }
    let(:user){ create(:user) }

    it 'can read all items' do
      expect(subject).to be_able_to(:read, Horse.new)
    end

    it 'can manage owned items' do
      @horse = create(:horse, user: user)
      expect(subject).to be_able_to(:update, @horse)
    end

    it 'cannot manage unowned items' do
      @horse = create(:horse)
      expect(subject).to_not be_able_to(:update, @horse)
    end
  end
end
