require 'rails_helper'

RSpec.describe Entry, type: :model do
  describe 'associations' do
    it 'belongs to klass' do
      expect(subject).to belong_to :klass
    end

    it 'belongs to horse' do
      expect(subject).to belong_to :horse
    end
  end
end
