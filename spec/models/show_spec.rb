require 'rails_helper'

RSpec.describe Show, type: :model do

  it_behaves_like "ownable"

  describe 'validations' do
    it 'validates presence of user_id' do
      expect(subject).to validate_presence_of :user_id
    end

    it 'validates presence of stable_id' do
      expect(subject).to validate_presence_of :stable_id
    end

    it 'validates presence of name' do
      expect(subject).to validate_presence_of :name
    end

    it 'validates presence of held_at' do
      expect(subject).to validate_presence_of :held_at
    end

    it 'requires datetime for held_at' do
      expect(subject).to allow_value(Time.now).for :held_at
      expect(subject).to_not allow_value("Time.now").for :held_at
      expect(subject).to_not allow_value(Time.now.to_i).for :held_at
    end

    it 'validates length of name' do
      expect(subject).to ensure_length_of(:name).is_at_least(4).is_at_most(30)
    end

    it 'validates presence of deadline' do
      expect(subject).to validate_presence_of :deadline
    end

    it 'validates numericality of deadline' do
      expect(subject).to validate_numericality_of :deadline
    end

    it 'validates presence of generator' do
      expect(subject).to validate_presence_of :generator
    end

    it 'defines an enumerated value for generator' do
      expect(subject).to define_enum_for :generator
    end
  end

  describe 'callbacks' do
    it 'starts job after save' do
      expect(subject).to callback(:create_job).after(:create)
    end
  end

  describe 'associations' do
    it 'belongs to user' do
      expect(subject).to belong_to(:user)
    end

    it 'belongs to stable' do
      expect(subject).to belong_to(:stable)
    end

    it 'has many klasses' do
      expect(subject).to have_many(:klasses)
    end
  end

  describe 'methods' do
    describe '.active?' do
      it 'deteremines whether show is unrun' do
        expect(build(:show).active?).to be true
        expect(build(:show, held_at: 3.days.ago).active?).to be false
        expect(build(:show, held_at: 8.hours.from_now, deadline: 12.hours).active?).to be true
      end
    end

    describe '.open?' do
      it 'determines whether show can be entered' do
        expect(build(:show).open?).to be true
        expect(build(:show, held_at: 3.days.ago).open?).to be false
        expect(build(:show, held_at: 8.hours.from_now, deadline: 12.hours).open?).to be false
      end
    end

    describe '.destructable?' do
      it 'returns false if show has already run' do
        expect(build(:show, held_at: 1.day.ago).destructable?).to be false
      end

      it 'returns false if show has entrants' do
        @show = create(:show)
        @show.klasses << create(:klass)
        @show.klasses.first.horses << create(:horse)
        expect(@show.destructable?).to be false
      end

      it 'returns true otherwise' do
        expect(build(:show).destructable?).to be true
      end
    end

    describe '.conduct' do
      it 'returns false if show has already run' do
        @show = create(:show)
        @klass = create(:klass, show: @show)
        @horse = create(:horse)
        @result = create(:result, {horse: @horse, klass: @klass})
        expect(@show.conduct).to be false
      end

      it 'judges each show if it has not been run yet' do
        @show = create(:show)
        2.times do
          @show.klasses << create(:klass)
          3.times { @show.klasses.first.horses << create(:horse) }
        end
        @show.conduct
        expect(@show.klasses.sample.judged).to be true
      end
    end
  end
end