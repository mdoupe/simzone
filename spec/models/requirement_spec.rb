require 'rails_helper'

RSpec.describe Requirement, type: :model do
  describe 'validates' do
    it 'presence of klass' do
      expect(subject).to validate_presence_of :klass_id
    end

    it 'presence of quality' do
      expect(subject).to validate_presence_of :quality
    end

    it 'presence of inclusive' do
      expect(subject).to validate_presence_of :inclusive
    end

    it 'presence of values' do
      expect(subject).to validate_presence_of :values
    end
  end

  describe 'associations' do
    it 'belongs to klass' do
      expect(subject).to belong_to :klass
    end
  end

  describe 'methods' do
    describe '.qualify?' do
      context 'given qualifying horse' do
        it 'returns true for color' do
          @horse = create(:horse, color: "Bay")
          @req = create(:requirement)
          expect(@req.qualify?(@horse)).to be true
        end

        it 'returns true for year' do
          @horse = create(:horse, year: 2011)
          @req = create(:requirement, {quality: "year", values: "2011,2012"})
          expect(@req.qualify?(@horse)).to be true
        end

        it 'returns true for breed' do
          @horse = create(:horse)
          @req = create(:requirement, {quality: "breed", values: "percheron,clydesdale"})
          expect(@req.qualify?(@horse)).to be true
        end
      end

      context 'given non-qualifying horse' do
        it 'returns false for color' do
          @horse = create(:horse, color: "Grey")
          @req = create(:requirement)
          expect(@req.qualify?(@horse)).to be false
        end

        it 'returns false for year' do
          @horse = create(:horse, year: 2010)
          @req = create(:requirement, {quality: "year", values: "2011,2012"})
          expect(@req.qualify?(@horse)).to be false
        end

        it 'returns false for breed' do
          @horse = create(:horse, breed: "Arabian")
          @req = create(:requirement, {quality: "breed", values: "percheron,clydesdale"})
          expect(@req.qualify?(@horse)).to be false
        end
      end
    end

    describe '.owned_by?' do
      before :each do
        @klass = create(:klass)
      end

      it 'reports true for shows owner' do
        expect(build(:requirement, klass: @klass).owned_by?(@klass.show.user)).to be true
      end

      it 'returns false for any other value' do
        expect(build(:requirement, klass: @klass).owned_by?(nil)).to be false
        expect(build(:requirement, klass: @klass).owned_by?("text")).to be false
        expect(build(:requirement, klass: @klass).owned_by?(@klass.show.user.id)).to be false
        expect(build(:requirement, klass: @klass).owned_by?(create(:user))).to be false
      end
    end

    describe '.destructable?' do
      before :each do
        @req = create(:requirement)
      end

      it 'returns true for empty, open klass' do
        expect(@req.destructable?).to be true
      end

      it 'returns false for non-empty klass' do
        @req.klass.horses << create(:horse)
        expect(@req.destructable?).to be false
      end

      it 'returns false for run klasses' do
        @req.klass.show.held_at = 1.day.ago
        @req.klass.show.save
        expect(@req.destructable?). to be false
      end
    end
  end
end
