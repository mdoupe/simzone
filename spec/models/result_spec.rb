require 'rails_helper'

RSpec.describe Result, type: :model do
  describe 'validates' do
    it 'klass for presence' do
      expect(subject).to validate_presence_of(:klass)
    end

    it 'horse for presence' do
      expect(subject).to validate_presence_of(:horse_id)
    end

    it 'horse for uniqueness scoped to klass' do
      expect(subject).to validate_uniqueness_of(:horse_id).scoped_to(:klass_id).with_message("can only be entered once per class")
    end

    it 'finish for presence' do
      expect(subject).to validate_presence_of(:finish)
    end

    it 'finish for uniqueness scoped to klass' do
      expect(subject).to validate_uniqueness_of(:finish).scoped_to(:klass_id).with_message("can only be ranked once per class")
    end
  end

  describe 'associations' do
    it 'belongs_to horse' do
      expect(subject).to belong_to :horse
    end

    it 'belongs_to horse' do
      expect(subject).to belong_to :klass
    end
  end
end
