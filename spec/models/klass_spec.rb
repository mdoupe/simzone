require 'rails_helper'

RSpec.describe Klass, type: :model do
  describe 'validates' do
    it 'presence of show' do
      expect(subject).to validate_presence_of :show_id
    end

    it 'presence of name' do
      expect(subject).to validate_presence_of :name
    end

    it 'length of name' do
      expect(subject).to ensure_length_of(:name).is_at_least(5).is_at_most(30)
    end

    it 'presence of entry_limit' do
      expect(subject).to validate_presence_of :entry_limit
    end

    it 'numericality of entry_limit' do
      expect(subject).to validate_numericality_of :entry_limit
    end
  end

  describe 'associations' do
    it 'belongs to show' do
      expect(subject).to belong_to :show
    end

    it 'has many requirements' do
      expect(subject).to have_many :requirements
    end

    it 'has many entries' do
      expect(subject).to have_many :entries
    end

    it 'has and belongs to many horses' do
      expect(subject).to have_many(:horses).through(:entries)
    end
  end

  describe "methods" do
    describe '.destructable?' do
      it 'returns false if klass has been judged' do
        expect(build(:judged_klass).destructable?).to be false
      end

      it 'returns false if klass has entrants' do
        @klass = create(:klass)
        @klass.horses << create(:horse)
        expect(@klass.destructable?).to be false
      end

      it 'returns true otherwise' do
        expect(build(:klass).destructable?).to be true
      end
    end

    describe '.owned_by?' do
      before :each do
        @show = create(:show)
      end

      it 'reports true for shows owner' do
        expect(build(:klass, show: @show).owned_by?(@show.user)).to be true
      end

      it 'returns false for any other value' do
        expect(build(:klass, show: @show).owned_by?(nil)).to be false
        expect(build(:klass, show: @show).owned_by?("text")).to be false
        expect(build(:klass, show: @show).owned_by?(@show.user.id)).to be false
        expect(build(:klass, show: @show).owned_by?(create(:user))).to be false
      end
    end

    describe '.eligible?' do
      before :each do
        @klass = create(:klass)
        create(:requirement, {quality: "breed", values: "arabian", klass: @klass})
        create(:requirement, {quality: "color", values: "bay,chestnut", klass: @klass})
      end

      it 'returns true for valid horse' do
        @horse = create(:horse,{breed: "Arabian", color: "Bay"})
        expect(@klass.eligible?(@horse)).to be true
      end

      it 'returns false for invalid horse' do
        @horse = create(:horse,{breed: "Arabian", color: "Black"})
        expect(@klass.eligible?(@horse)).to be false
      end

      it 'returns false for non-horse' do
        @horse = "text"
        expect(@klass.eligible?(@horse)).to be false
      end

      it 'returns false for already entered horse' do
        @horse = create(:horse,{breed: "Arabian", color: "Bay"})
        @klass.horses << @horse
        expect(@klass.eligible?(@horse)).to be false
      end
    end

    describe '.open?' do
      context 'given open show' do
        before :each do
          @show = create(:show, {held_at: 7.days.from_now, deadline: 12.hours.to_i})
          @klass = create(:klass, {show: @show, entry_limit: 10})
        end

        it 'returns true if under entry limit' do
          3.times do
            @klass.horses << create(:horse)
          end
          expect(@klass.open?).to be true
        end

        it 'returns false if at entry limit' do
          10.times do
            @klass.horses << create(:horse)
          end
          expect(@klass.open?).to be false
        end
      end

      context 'given closed show' do
        before :each do
          @show = create(:inactive_show)
          @klass = create(:klass, show: @show)
        end

        it 'returns false' do
          expect(@klass.open?).to be false
        end
      end
    end

    describe '.full?' do
      before :each do
        @klass = create(:klass, entry_limit: 5)
      end

      context 'given full klass' do
        before :each do
          5.times do
            @klass.horses << create(:horse)
          end
        end

        it 'returns true' do
          expect(@klass.full?).to be true
        end
      end
    end

    describe '.entered?' do
      before :each do
        @klass = create(:klass)
      end

      context 'if klass has entries' do
        before :each do
          @klass.horses << create(:horse)
        end

        it 'returns true' do
          expect(@klass.entered?).to be true
        end
      end

      context 'if klass has no entries' do
        it 'returns false' do
          expect(@klass.entered?).to be false
        end

      end
    end

    describe '.enter' do
      before :each do
        @klass = create(:klass)
        create(:requirement, {klass: @klass, quality: "breed", values: "percheron"})
      end

      context 'given valid animal' do
        before :each do
          @horse = create(:horse)
        end

        it 'enters horse' do
          @klass.enter(@horse)
          @klass.reload
          expect(@klass.horses).to include @horse
        end

        it 'returns true' do
          expect(@klass.enter(@horse)).to be true
        end
      end

      context 'given invalid animal' do
        before :each do
          @horse = create(:horse, breed: "Clydesdale")
        end

        it 'does not enter horse' do
          @klass.enter(@horse)
          @klass.reload
          expect(@klass.horses).to_not include @horse
        end
      end
    end
  end

  describe 'results methods' do
    describe '.judge' do
      context 'given open show' do
        before :all do
          @show = create(:show, generator: "standard")
          @klass = create(:klass, show: @show)
          @klass.requirements << create(:requirement, {quality: "breed", values: "Arabian"})
          @klass.horses << create(:horse, {name: "Horsie One", breed: "Arabian"})
          @klass.horses << create(:horse, {name: "Horsie Two", breed: "Arabian"})
          @klass.judge
          @klass.reload
        end

        it 'deletes the entries' do
          expect(@klass.entries.count).to be 0
        end

        it 'creates the results' do
          expect(@klass.results.count).to be 2
        end

        it 'sets the klass judged' do
          expect(@klass.judged).to be true
        end
      end
    end
  end
end