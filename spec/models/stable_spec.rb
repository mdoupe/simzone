require 'rails_helper'

RSpec.describe Stable, type: :model do

  it_behaves_like "ownable"

  describe 'validates' do
    it 'presence of name' do
      expect(subject).to validate_presence_of(:name)
    end

    it 'length of name' do
      expect(subject).to ensure_length_of(:name).is_at_least(4).is_at_most(50)
    end

    it 'presence of user_id' do
      expect(subject).to validate_presence_of(:user_id)
    end
  end

  describe 'associations' do
    it 'belongs to users' do
      expect(subject).to belong_to :user
    end

    it 'has many horses' do
      expect(subject).to have_many :horses
    end

    it 'has many shows' do
      expect(subject).to have_many :shows
    end
  end
end
