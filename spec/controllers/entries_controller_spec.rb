require 'rails_helper'

RSpec.describe EntriesController, type: :controller do

  describe "GET #new" do
    before :all do
      @user = create(:user)
      @klass = create(:klass)
    end

    context 'while logged in' do
      before :each do
        session[:user_id] = @user.id
        get :new, class_id: @klass.id
      end

      it 'returns success' do
        expect(response).to have_http_status(:success)
      end

      it 'renders the new template' do
        expect(response).to render_template :new
      end

      it 'assigns the klass to @klass' do
        expect(assigns[:klass]).to eq @klass
      end

      it 'assigns a new entry to @entry' do
        expect(assigns[:entry]).to be_a_new Entry
      end
    end

    context 'while not logged in' do
      before :each do
        get :new, class_id: @klass.id
      end

      it 'redirects to login' do
        expect(response).to redirect_to new_session_url
      end
    end
  end

  describe 'POST #create' do
    before :all do
      @klass = create(:klass)
      @horse = create(:horse)
    end

    context 'while logged in' do
      context 'to horse owner' do
        before :each do
          session[:user_id] = @horse.user_id
          post :create, {class_id: @klass.id, entry: {horse_id: @horse.id}}
          @klass.reload
        end

        it 'enters the horse' do
          expect(@klass.horses).to include(@horse)
        end

        it 'redirects to class' do
          expect(response).to redirect_to class_url(@klass)
        end
      end

      context 'to different user' do
        before :each do
          session[:user_id] = create(:user).id
          post :create, {class_id: @klass.id, entry: {horse_id: @horse.id}}
          @klass.reload
        end

        it 'does not enter the horse' do
          expect(@klass.horses).to_not include(@horse)
        end

        it 'renders new template' do
          expect(response).to render_template :new
        end
      end
    end

    context 'while not logged in' do
      before :each do
        get :new, class_id: @klass.id
      end

      it 'redirects to login' do
        expect(response).to redirect_to new_session_url
      end
    end
  end

  describe 'DELETE #destroy' do
    before :all do
      @klass = create(:klass)
      @horse = create(:horse)
    end

    before :each do
      @entry = create(:entry, horse: @horse, klass: @klass)
    end

    context 'while logged in' do
      context 'to horse owner' do
        before :each do
          session[:user_id] = @horse.user_id
          delete :destroy, id: @entry.id
        end

        it 'deletes the entry' do
          expect(Entry.where(id: @entry.id)).to eq []
        end

        it 'redirects to class' do
          expect(response).to redirect_to class_url(@klass)
        end
      end

      context 'to different user' do
        before :each do
          session[:user_id] = create(:user).id
          delete :destroy, id: @entry.id
        end

        it 'does not delete the entry' do
          expect(Entry.find(@entry.id)).to eq @entry
        end

        it 'redirects to class' do
          expect(response).to redirect_to class_url(@klass)
        end
      end
    end

    context 'while not logged in' do
      it 'redirects to login' do
        delete :destroy, id: @entry.id
        expect(response).to redirect_to new_session_url
      end
    end
  end

end
