require 'rails_helper'

RSpec.describe ShowsController, type: :controller do

  describe "GET #index" do
    before :each do
      session[:user_id] = create(:user).id
      get :index
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'renders the index template' do
      expect(response).to render_template :index
    end

    it 'assigns all shows to the @shows variable' do
      expect(assigns[:shows].size).to eq(Show.count)
    end

    it 'decorates the collection' do
      @show = create(:show)
      expect(assigns[:shows]).to be_decorated
    end
  end

  describe "GET #show" do
    before :each do
      @show = create(:show)
      session[:user_id] = @show.user_id
      get :show, id: @show.id
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'renders the index template' do
      expect(response).to render_template :show
    end

    it 'assigns all shows to the @shows variable' do
      expect(assigns[:show]).to eq(@show)
    end

    it 'decorates the collection' do
      expect(assigns[:show]).to be_decorated
    end
  end

  describe "GET #new" do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
        get :new
      end

      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      it 'renders the new template' do
        expect(response).to render_template :new
      end

      it 'assigns a new Show to @show' do
        expect(assigns[:show]).to be_a_new Show
      end

      it 'decorates @show' do
        expect(assigns[:show]).to be_decorated
      end
    end

    include_examples "controller_bounce_login_get" do let(:action) { :new } end

  end

  describe "POST #create" do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
        @stable = create(:stable, user: @user)
      end

      context 'given valid data' do
        before :each do
          @show = attributes_for(:show, user_id: @user.id, stable_id: @stable.id)
        end

        it 'saves show to the database' do
          expect{
            post :create, show: @show
          }.to change(Show,:count).by(1)
        end

        it 'redirects to show view' do
          post :create, show: @show
          expect(response).to redirect_to show_url(Show.last)
        end

        it 'assigns the new show to the logged in user' do
          @show[:user_id] = create(:user).id
          post :create, show: @show
          expect(Show.last.user_id).to eq(session[:user_id])
        end
      end

      context 'given invalid information' do
        before :each do
          @show = attributes_for(:invalid_show, user_id: @user.id, stable_id: @stable_id)
        end

        it 'does not save the show' do
          expect{
            post :create, show: @show
          }.to_not change(Show,:count)
        end

        it 'renders the new template' do
          post :create, show: @show
          expect(response).to render_template :new
        end
      end
    end

    include_examples "controller_bounce_login_post" do let(:action) { :create } end
  end

  describe "GET #edit" do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
      end

      context 'to show owner' do
        before :each do
          @show = create(:show, user: @user)
          get :edit, id: @show.id
        end

        it "returns http success" do
          expect(response).to have_http_status(:success)
        end

        it 'renders the edit template' do
          expect(response).to render_template :edit
        end

        it 'assigns the show to @show' do
          expect(assigns[:show]).to eq(@show)
        end

        it 'decorates the show' do
          expect(assigns[:show]).to be_decorated
        end
      end

      context 'to different user' do
        before :each do
          @show = create(:show)
          get :edit, id: @show.id
        end

        it 'redirects to show view' do
          expect(response).to redirect_to show_url(@show)
        end
      end

    end
    
    include_examples "controller_bounce_login_get" do let(:action) { :new } end
  end

  describe "POST #update" do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        @show = create(:show, user: @user)
      end

      context 'to the record owner' do
        before :each do
          session[:user_id] = @user.id
        end

        context 'given valid information' do
          before :each do
            post :update, {id: @show.id, show: attributes_for(:show, name: "Different Show Name")}
          end

          it 'updates the show' do
            @show.reload
            expect(@show.name).to eq("Different Show Name")
          end

          it 'redirects to the show page' do
            expect(response).to redirect_to show_url(@show)
          end
        end

        context 'given invalid info' do
          before :each do
            post :update, {id: @show.id, show: attributes_for(:invalid_show)}
          end

          it 'does not save the show' do
            @show.reload
            expect(@show.name).to eq(build(:show).name)
          end

          it 'renders the edit template' do
            expect(response).to render_template :edit
          end
        end
      end

      context 'to a different user' do
        before :each do
          @logged = create(:user)
          session[:user_id] = @logged.id
          post :update, {id: @show.id, show: attributes_for(:show, name: "A Different Show Name")}
        end

        it 'does not update the show' do
          @show.reload
          expect(@show.name).to_not eq("A Different Show Name")
        end

        it 'redirects to show view' do
          expect(response).to redirect_to @show
        end
      end
    end

    include_examples "controller_bounce_login_post" do let(:action) { :update } end
  end

  describe "DELETE #destroy" do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
      end

      context 'to the record owner' do
        context "given a destructable show" do
          before :each do
            @show = create(:destructable_show, user: @user)
            delete :destroy, id: @show.id
          end

          it 'destroys record' do
            expect(Show.where(id: @show.id).size).to be 0
          end

          it 'redirects to show list' do
            expect(response).to redirect_to shows_url
          end
        end

        context 'given an indestructable show' do
          before :each do
            @show = create(:indestructable_show, user: @user)
            delete :destroy, id: @show.id
          end
          
          it 'does not delete the show' do
            expect(Show.where(id: @show.id).size).to be 1
          end

          it 'redirects to show view' do
            expect(response).to redirect_to @show
          end
        end
      end

      context 'to different user' do
        before :each do
          @show = create(:show)
          delete :destroy, id: @show.id
        end

        it 'does not delete the record' do
          expect(Show.where(id: @show.id).size).to be 1
        end

        it 'redirects to the show view' do
          expect(response).to redirect_to @show
        end
      end
    end

    include_examples "controller_bounce_login_post" do let(:action) { :destroy } end
  end

  describe 'GET mine' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
        3.times do
          create(:show, user: @user)
        end
        2.times do
          create(:alt_show)
        end
        get :mine
      end

      it 'collects my shows' do
        expect(assigns[:shows].size).to eq(3)
      end

      it 'renders the mine template' do
        expect(response).to render_template :mine
      end
    end

    include_examples "controller_bounce_login_get" do let(:action) { :mine } end
  end

  describe "GET active" do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
        3.times do
          create(:show)
        end
        2.times do
          create(:inactive_show)
        end
        get :active
      end

      it 'lists only active shows' do
        Show.all do |show|
          if show.active
            expect(assigns[:shows]).to include(show)
          else
            expect(assigns[:shows]).to_not include(show)
          end
        end
      end

      it 'renders the :active template' do
        expect(response).to render_template :active
      end
    end

    include_examples "controller_bounce_login_get" do let(:action) { :active } end
  end
end
