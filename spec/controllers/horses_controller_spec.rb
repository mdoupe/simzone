require 'rails_helper'

RSpec.describe HorsesController, type: :controller do

  describe "GET #new" do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
      end
      it "returns http success" do
        get :new
        expect(response).to have_http_status(:success)
      end

      it 'renders the new template' do
        get :new
        expect(response).to render_template(:new)
      end

      it 'sets @horse to a new horse' do
        get :new
        expect(assigns(:horse)).to be_a_new(Horse)
      end

      it 'decorates the horse record' do
        get :new
        expect(assigns(:horse)).to be_decorated
      end
    end

    context 'while not logged in' do
      before :each do
        session[:user_id] = nil
      end
      it 'redirects to login page' do
        get :new
        expect(response).to redirect_to(new_session_url)
      end
    end
  end

  describe "POST #create" do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
        @stable = create(:stable, user: @user)
      end

      context 'given valid data' do
        before :each do
          @horse = attributes_for(:horse, stable: @stable)
          @horse[:stable_id] = @stable.id
        end

        it 'saves the new horse to the database' do
          expect{
            post :create, { horse: @horse }
          }.to change(Horse, :count).by(1)
        end

        it 'assigns the horse to the currently logged in user' do
          post :create, { horse: @horse }
          @horse = assigns(:horse)
          expect(@horse.user_id).to eq(@user.id)
        end

        it 'redirects to horse page' do
          post :create, {horse: @horse}
          expect(response).to redirect_to(Horse.last)
        end
      end

      context 'given invalid data' do
        before :each do
          @horse = attributes_for(:invalid_horse)
        end

        it 'does not save the horse' do
          expect {
            post :create, horse: @horse
          }.to_not change(Horse, :count)
        end

        it 'renders the new template' do
          post :create, horse: @horse
          expect(response).to render_template :new
        end
      end
    end

  end

  describe "GET #edit" do
    context "while logged in" do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
      end

      context 'to correct user' do
        before :each do
          @horse = create(:horse, user: @user)
          get :edit, id: @horse.id
        end

        it "returns http success" do
          expect(response).to have_http_status(:success)
        end

        it 'renders the edit template' do
          expect(response).to render_template(:edit)
        end

        it 'assigns the horse to @horse variable' do
          expect(assigns(:horse)).to eq(@horse)
        end

        it 'decorates the horse record' do
          expect(assigns(:horse)).to be_decorated
        end
      end

      context 'to wrong user' do
        before :each do
          @owner = create(:user)
          @horse = create(:horse, user: @owner)
          get :edit, id: @horse.id
        end

        it 'redirects to horse page' do
          expect(response).to redirect_to horse_url(@horse)
        end
      end
    end
  end

  describe "GET #update" do
    context 'while logged in' do
      before :each do
        @owner = create(:user)
        @horse = create(:horse, user: @owner)
      end

      context 'to the correct user' do
        before :each do
          session[:user_id] = @owner.id
        end

        context "given valid info" do
          before :each do
            post :update, {horse: attributes_for(:bert_horse, user: @owner), id: @horse.id}
            @horse.reload
          end

          it 'updates the record' do
            expect(@horse.name).to eq("Bert")
          end

          it 'redirects to the record view' do
            expect(response).to redirect_to(horse_url(@horse))
          end
        end

        context 'given invalid info' do
          before :each do
            post :update, {horse: attributes_for(:invalid_horse, user: @owner), id: @horse.id}
            @horse.reload
          end

          it 'does not update the record' do
            expect(@horse.name).to eq("Horsie")
          end

          it 'renders the edit template' do
            expect(response).to render_template :edit
          end
        end
      end

      context 'to the wrong user' do
        before :each do
          @logged = create(:user)
          session[:user_id] = @logged.id
          post :update, {horse: attributes_for(:bert_horse, user: @owner), id: @horse.id}
          @horse.reload
        end

        it 'does not update horse' do
          expect(@horse.name).to_not eq("Bert")
        end

        it 'redirects to horse page' do
          expect(response).to redirect_to(horse_url(@horse))
        end
      end
    end

    context 'while not logged in' do
      before :each do
        @horse = create(:horse)
        session[:user_id] = nil
        post :update, id: @horse.id
      end

      it 'redirects to login page' do
        expect(response).to redirect_to new_session_url
      end
    end
  end

  describe "GET #show" do
    before :each do
      @horse = create(:horse)
      get :show, id: @horse.id
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'assigns the requested record to the @horse variable' do
      expect(assigns[:horse]).to eq(@horse)
    end

    it 'decorates the horse record' do
      expect(assigns[:horse]).to be_decorated
    end
  end

  describe "GET #index" do
    before :each do
      3.times do
        create(:horse)
      end
      get :index
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'assigns the full list of records to @horses' do
      expect(assigns[:horses].size).to eq Horse.count
    end

    it 'decorates the horse records' do
      expect(assigns[:horses].sample).to be_decorated
    end
  end

  describe 'GET #mine' do
    before :each do
      @other_user = create(:user)
      @logged = create(:user)
      3.times do
        create(:horse, user: @other_user)
      end
      2.times do
        create(:horse, user: @logged)
      end
    end
    
    context 'while logged in' do
      before :each do
        session[:user_id] = @logged.id
        get :mine
      end

      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      it 'fetches only those horses owned by the logged in user' do
        expect(assigns[:horses].size).to equal(2)
      end

      it 'decorates the horse records' do
        expect(assigns[:horses].sample).to be_decorated
      end
    end

    context 'while not logged in' do
      before :each do
        get :mine
      end

      it 'redirects to login page' do
        expect(response).to redirect_to(new_session_url)
      end
    end
  end

  describe 'GET #transfer' do
    before :each do
      @owner = create(:user)
      @horse = create(:horse, user: @owner)
    end

    context 'while logged in' do
      context 'as the records owner' do
        before :each do
          session[:user_id] = @owner.id
          get :transfer, id: @horse.id
        end

        it 'returns http success' do
          expect(response).to have_http_status(:success)
        end

        it 'renders the transfer template' do
          expect(response).to render_template(:transfer)
        end

        it 'assigns the record to @horse' do
          expect(assigns[:horse]).to eq(@horse)
        end

        it 'decorates the horse record' do
          expect(assigns[:horse]).to be_decorated
        end
      end

      context 'as a different user' do
        before do
          @logged = create(:user)
          session[:user_id] = @logged.id
          get :transfer, id: @horse.id
        end

        it 'redirects to the record view' do
          expect(response).to redirect_to horse_url(@horse)
        end
      end
    end

    context 'while not logged in' do
      before :each do
        @horse = create(:horse)
        session[:user_id] = nil
        get :transfer, id: @horse.id
      end

      it 'redirects to login page' do
        expect(response).to redirect_to new_session_url
      end
    end
  end

  describe 'POST #give' do
      before :each do
        @owner = create(:user)
        @new_owner = create(:user)
        @horse = create(:horse, user: @owner)
      end
    context 'while logged in' do
      context 'to the record owner' do
        before :each do
          session[:user_id] = @owner.id
          post :give, {id: @horse.id, horse: {user_id: @new_owner.id}}
        end

        it 'updates the record owner' do
          @horse.reload
          expect(@horse.user_id).to equal(@new_owner.id)
        end

        it 'redirects to the record view' do
          expect(response).to redirect_to horse_url(@horse)
        end
      end

      context 'to the wrong user' do
        before :each do
          session[:user_id] = @new_owner.id
          post :give, {id: @horse.id, horse: {user_id: @new_owner.id}}
        end

        it 'redirects to the record view' do
          expect(response).to redirect_to horse_url(@horse)
        end
      end
    end

    context 'while not logged in' do
      before :each do
        session[:user_id] = nil
        post :give, {id: @horse.id, horse: {user_id: @new_owner.id}}
      end

      it 'redirects to login page' do
        expect(response).to redirect_to new_session_url
      end
    end
  end

  describe 'GET #stable' do
    before :each do
      @owner = create(:user)
      @horse = create(:horse, user: @owner)
      @stable = create(:stable)
    end

    context 'while logged in' do
      context 'to the record owner' do
        before :each do
          session[:user_id] = @owner.id
          get :stable, id: @horse.id
        end

        it 'returns http success' do
          expect(response).to have_http_status(:success)
        end

        it 'renders the stable template' do
          expect(response).to render_template :stable
        end

        it 'assigns the correct horse to @horse' do
          expect(assigns[:horse]).to eq(@horse)
        end

        it 'decorates @horse' do
          expect(assigns[:horse]).to be_decorated
        end
      end

      context 'to the wrong user' do
        before :each do
          @logged = create(:user)
          session[:user_id] = @logged.id
          get :stable, id: @horse.id
        end

        it 'returns to view page' do
          expect(response).to redirect_to horses_url(@horse)
        end
      end
    end

    context 'while not logged in' do
      before :each do
        session[:user_id] = nil
      end
      it 'redirects to login page' do
        get :stable, id: Horse.first.id
        expect(response).to redirect_to(new_session_url)
      end
    end
  end

  describe 'POST #house' do
    context 'while logged in' do
      before :each do
        @logged = create(:user)
        session[:user_id] = @logged.id
        @stable = create(:stable)
      end

      context 'to the horse owner' do
        before :each do
          @horse = create(:horse, user: @logged)
        end

        context 'given valid information' do
          before :each do
            post :house, {horse: {stable_id: @stable.id}, id: @horse.id}
          end

          it 'updates the horse' do
            @horse.reload
            expect(@horse.stable).to eq(@stable)
          end

          it 'redirects to horse page' do
            expect(response).to redirect_to horse_url(@horse)
          end
        end

        context 'given invalid information' do
          before :each do
            post :house, {horse: {stable_id: nil}, id: @horse.id}
          end

          it 'does not update the horse' do
            @horse.reload
            expect(@horse.stable).to_not eq(nil)
          end

          it 'renders the stable template' do
            expect(response).to render_template :stable
          end
        end
      end

      context 'to a different user' do
        before :each do
          @horse = create(:horse)
          post :house, id: @horse.id
        end

        it 'redirects to view page' do
          expect(response).to redirect_to horse_url(@horse)
        end
      end
    end

    context 'while not logged in' do
      before :each do
        session[:user_id] = nil
      end
      it 'redirects to login page' do
        post :house, id: create(:horse).id
        expect(response).to redirect_to(new_session_url)
      end
    end
  end
end