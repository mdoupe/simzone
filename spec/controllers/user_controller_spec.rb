require 'rails_helper'

RSpec.describe UserController, type: :controller do

  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end

    it 'renders the new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'sets @user to a new user' do
      get :new
      expect(assigns(:user)).to be_a_new(User)
    end
  end

  describe "POST #create" do
    context "given valid data" do
      it 'saves the new user to the database' do
        expect{
          post :create, user: attributes_for(:user)
        }.to change(User, :count).by(1)
      end

      it 'redirects to the front page' do
        post :create, user: attributes_for(:user)
        expect(response).to redirect_to(root_url)
      end
    end

    context 'given invalid data' do
      it 'does not save the new user to the database' do
        expect{
          post :create, user: attributes_for(:invalid_user)
        }.to_not change(User, :count)
      end

      it 'renders the new template' do
        post :create, user: attributes_for(:invalid_user)
        expect(response).to render_template(:new)
      end
    end
  end

  describe "GET #edit" do

    context 'when logged in as user' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
      end

      it "returns http success" do
        get :edit, id: @user
        expect(response).to have_http_status(:success)
      end

      it 'renders the edit template' do
        get :edit, id: @user
        expect(response).to render_template(:edit)
      end

      it 'assigns the indicated user to @user' do
        get :edit, id: @user
        expect(assigns(:user)).to eq(@user)
      end
    end

    context 'when different user logged in' do
      it 'redirects to own edit page' do
        user = create(:user)
        logged = create(:user)
        session[:user_id] = logged.id
        get :edit, id: user
        expect(response).to redirect_to(edit_user_url(logged))
      end
    end

    context 'when not logged in' do
      before :each do
        user = create(:user)
        session[:user_id] = nil
        get :edit, id: user
      end

      it 'redirects to login page' do
        expect(response).to redirect_to(new_session_url)
      end
    end
  end

  describe "POST #update" do
    before :each do
      @user = create(:user)
    end

    context 'when user is logged in' do
      before :each do
        session[:user_id] = @user.id
      end

      it 'assigns the indicated user to @user' do
        post :update, {id: @user, user: attributes_for(:user)}
        expect(assigns(:user)).to eq(@user)
      end

      context 'given valid data' do
        it 'saves the update to the record' do
          post :update, {id: @user, user: attributes_for(:user, name: "test")}
          @user.reload
          expect(@user.name).to eq("test")
        end

        it 'redirects to root' do
          put :update, {id: @user, user: attributes_for(:user)}
          expect(response).to redirect_to(root_url)
        end
      end

      context 'given invald data' do
        it 'does not update the record' do
          post :update, {id: @user, user: attributes_for(:user, name: "no")}
          @user.reload
          expect(@user.name).to_not eq("no")
        end

        it 'renders the edit template' do
          post :update, {id: @user, user: attributes_for(:user, name: "no")}
          expect(response).to render_template(:edit)
        end
      end
    end

    context 'when different user is logged in' do
      before :each do
        @logged = create(:user)
        session[:user_id] = @logged.id
      end

      it 'redirects to own edit page' do
        post :update, {id: @user, user: attributes_for(:user)}
        expect(response).to redirect_to(edit_user_url(@logged))        
      end
    end

    context 'when not logged in' do
      before :each do
        session[:user_id] = nil
      end

      it 'redirects to login page' do
        post :update, {id: @user, user: attributes_for(:user)}
        expect(response).to redirect_to(new_session_url)
      end
    end
  end

end
