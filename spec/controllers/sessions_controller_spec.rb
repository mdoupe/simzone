require 'rails_helper'

RSpec.describe SessionsController, type: :controller do

  describe "GET #new" do

    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end

    it 'renders the new template' do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe "POST #create" do
    context 'given valid user authentication' do
      before :each do
        @user = create(:user)
        post :create, { name: @user.name, password: @user.password }
      end

      it 'logs the user in' do
        expect(session[:user_id]).to equal(@user.id)
      end

      it 'redirects to root' do
        expect(response).to redirect_to(root_url)
      end
    end

    context 'given invalid user authentication' do
      before :each do
        @user = create(:user)
        post :create, { name: @user.name, password: "incorrect" }
      end

      it 'does not log the user in' do
        expect(session[:user_id]).to_not equal(@user.id)
      end

      it 'redirects back to login' do
        expect(response).to redirect_to(new_session_url)
      end
    end
  end

  describe "POST #destroy" do
    before :each do
      @user = create(:user)
      session[:user_id] = @user.id
      delete :destroy
    end

    it 'logs the user out' do
      expect(session[:user_id]).to_not equal(@user.id)
    end

    it 'redirects to root' do
      expect(response).to redirect_to(root_url)
    end
  end

end
