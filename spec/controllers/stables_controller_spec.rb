require 'rails_helper'

RSpec.describe StablesController, type: :controller do

  describe 'GET #new' do
    before :each do
      @user = create(:user)
    end

    context 'while logged in' do
      before :each do
        session[:user_id] = @user.id
        get :new
      end
      
      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      it 'renders the new template' do
        expect(response).to render_template :new
      end

      it 'assigns a new stable to @stable' do
        expect(assigns[:stable]).to be_a_new(Stable)
      end
    end

    context 'while not logged in' do
      before :each do
        session[:user_id] = nil
        get :new
      end

      it 'redirects to login' do
        expect(response).to redirect_to new_session_url
      end
    end
  end

  describe 'POST #create' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
        @stable = attributes_for(:stable)
      end

      context 'given valid data' do
        it 'saves the new stable' do
          expect{
            post :create, stable: @stable
          }.to change(Stable, :count).by(1)
        end

        it 'redirects to new stable view' do
          post :create, stable: @stable
          expect(response).to redirect_to stable_url(Stable.last)
        end

        it 'assigns stable to logged in user' do
          post :create, stable: @stable
          expect(Stable.last.user_id).to eq(@user.id)
        end
      end

      context 'given invalid information' do 
        before :each do
          @stable[:name] = "s"
        end

        it 'does not save the stable' do
          expect{
            post :create, stable: @stable
          }.to_not change(Stable, :count)
        end

        it 'renders the new template' do
          post :create, stable: @stable
          expect(response).to render_template :new
        end
      end
    end

    context 'while not logged in' do
      before :each do
        session[:user_id] = nil
      end

      it 'returns to login' do
        post :create, stable: attributes_for(:stable)
        expect(response).to redirect_to new_session_url 
      end
    end
  end

  describe 'GET #edit' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
      end
      context 'as the record owner' do
        before :each do
          @stable = create(:stable, user: @user)
          get :edit, id: @stable.id
        end

        it 'returns http success' do
          expect(response).to have_http_status(:success)
        end

        it 'renders the edit template' do
          expect(response).to render_template(:edit)
        end

        it 'assigns the stable to @stable' do
          expect(assigns[:stable]).to eq(@stable)
        end
      end

      context 'as a different user' do
        before :each do
          @logged = create(:user)
          session[:user_id] = @logged.id
          @stable = create(:stable)
          get :edit, id: @stable.id
        end

        it 'redirects to view' do
          expect(response).to redirect_to stable_url(@stable)
        end
      end
    end

    context 'while not logged in' do
      before :each do
        session[:user_id] = nil
        @stable = create(:stable)
      end

      it 'redirects to login' do
        get :edit, id: @stable.id
        expect(response).to redirect_to new_session_url
      end
    end
  end

  describe 'POST #update' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        session[:user_id] = @user.id
      end

      context 'to record owner' do
        before :each do
          @stable = create(:stable, user: @user)
        end

        context 'given valid info' do
          before :each do
            post :update, { id: @stable.id, stable: attributes_for(:alt_stable)}
          end

          it 'updates the record' do
            @stable.reload
            expect(@stable.name).to eq("Alternative Equine")
          end

          it 'redirects to stable view' do
            expect(response).to redirect_to stable_url(@stable)
          end
        end

        context 'given invalid info' do
          before :each do
            post :update, { id: @stable.id, stable: attributes_for(:invalid_stable)}
          end

          it 'renders the edit template' do
            expect(response).to render_template :edit
          end
        end
      end

      context 'to other user' do
        before :each do
          @stable = create(:stable)
          post :update, {id: @stable.id, stable: attributes_for(:alt_stable)}
        end

        it 'does not update the stable' do
          @stable.reload
          expect(@stable.name).to_not eq("Alternative Equine")
        end

        it 'redirects to stable view' do
          expect(response).to redirect_to stable_url(@stable)
        end
      end
    end

    context 'while not logged in' do
      before :each do
        @stable = create(:stable)
        session[:user_id] = nil
        post :update, {id: @stable.id, stable: attributes_for(:alt_stable)}
      end

      it 'redirects to login' do
        expect(response).to redirect_to new_session_url
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @stable = create(:stable)
      get :show, id: @stable.id
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'assigns requested record to @stable' do
      expect(assigns(:stable)).to eq(@stable)
    end

    it 'decorates the record' do
      expect(assigns[:stable]).to be_decorated
    end
  end

  describe 'GET #index' do
    before :each do
      3.times do
        create(:stable)
      end
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'assigns the collection of stables to @stable' do
      expect(assigns[:stables].size).to eq(Stable.count)
    end

    it 'decorates the collection' do
      expect(assigns[:stables].sample).to be_decorated
    end
  end

  describe 'DELETE :destroy' do
    before :each do
      @user = create(:user)
      @stable = create(:stable, user: @user)
    end

    context 'while logged in' do
      context 'to the owner' do
        before :each do
          session[:user_id] = @user.id
          delete :destroy, id: @stable.id
        end

        it 'destroys the stable' do
          expect(Stable.where(id: @stable.id).size).to eq(0)
        end

        it 'redirects to my stables' do
          expect(response).to redirect_to my_stables_url
        end
      end

      context 'to other user' do
        before :each do
          @logged = create(:user)
          session[:user_id] = @logged.id
          delete :destroy, id: @stable.id
        end

        it 'does not destroy the stable' do
          expect(Stable.find(@stable.id)).to eq(@stable)
        end

        it 'redirects to stable view' do
          expect(response).to redirect_to stable_url(@stable)
        end
      end
    end

    context 'while not logged in' do
      before :each do
        @stable = create(:stable)
        delete :destroy, id: @stable.id
      end

      it 'redirects to login' do
        expect(response).to redirect_to new_session_url
      end
    end
  end

  describe 'GET #mine' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        @my_stable = create(:stable, user: @user)
        @not_mine = create(:alt_stable)
        session[:user_id] = @user.id
        get :mine
      end

      it 'returns http success' do
        expect(response).to have_http_status :success
      end

      it 'renders the mine template' do
        expect(response).to render_template :mine
      end

      it 'assigns my stables to the @stables variable' do
        expect(assigns[:stables]).to include @my_stable
      end

      it 'does not include other stables in @stables' do
        expect(assigns[:stables]).to_not include @not_mine
      end

      it 'decorates the collection' do
        expect(assigns[:stables]).to be_decorated
      end
    end

    context 'while not logged in' do
      before :each do
        get :mine
      end

      it 'redirects to login' do
        expect(response).to redirect_to new_session_url
      end
    end
  end
end
