require 'rails_helper'

RSpec.describe KlassesController, type: :controller do

  describe "GET #show" do
    before :all do
      @klass = create(:klass)
      @user = create(:user)
    end

    before :each do
      session[:user_id] = @user.id
      get :show, id: @klass.id
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'renders the show template' do
      expect(response).to render_template :show
    end

    it 'assigns correct klass to @klass' do
      expect(assigns[:klass]).to eq(@klass)
    end

    it 'decorates @klass' do
       expect(assigns[:klass]).to be_decorated
    end
  end

  describe "GET #new" do
    before :all do
      @user = create(:user)
    end

    context 'while logged in to show owner' do
      before :all do
        @show = create(:show, user: @user)
      end
   
      before :each do
        session[:user_id] = @user.id
        get :new, show_id: @show.id
      end
   
      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it 'renders the new template' do
        expect(response).to render_template :new
      end

      it 'assigns a new klass to @klass' do
        expect(assigns[:klass]).to be_a_new(Klass)
      end

      it 'decorates @klass' do
        expect(assigns[:klass]).to be_decorated
      end
    end

    context 'while logged in to a different user' do
      before :all do
        @show = create(:show)
      end

      before :each do
        session[:user_id] = @user.id
        get :new, show_id: @show.id
      end

      it 'redirects to show view' do
        expect(response).to redirect_to show_url(@show)
      end
    end
  end

  describe "POST #create" do
    before :all do
      @user = create(:user)
    end

    before :each do
      session[:user_id] = @user.id
    end

    context 'while logged in as show owner' do
      before :all do
        @show = create(:show, user: @user)
      end

      context 'given valid data' do
        before :all do
          @klass = attributes_for(:klass)
        end

        it 'saves klass to database' do
          expect{
            post :create, { show_id: @show.id, klass: @klass }
          }.to change(Klass,:count).by(1)
        end

        it 'redirects to show view' do
          post :create, {show_id: @show.id, klass: @klass}
          expect(response).to redirect_to show_url(@show)
        end
      end

      context 'given invalid data' do
        before :all do
          @klass = attributes_for(:invalid_klass)
        end

        it 'does not save klass to database' do
          expect{
            post :create, {show_id: @show.id, klass: @klass}
          }.to_not change(Klass,:count)
        end
      end
    end

    context 'while logged in to different user' do
      before :all do
        @show = create(:show)
        @klass = attributes_for(:klass, show: @show)
      end

      before :each do
        post :create, {show_id: @show.id, klass: @klass}
      end

      it 'redirects to show view' do
        expect(response).to redirect_to show_url(@show)
      end
    end
  end

  describe "GET #edit" do
    before :all do
      @user = create(:user)
    end

    context 'while logged in as show owner' do
      before :all do
        @show = create(:show, user: @user)
        @klass = create(:klass, show: @show)
      end

      before :each do
        session[:user_id] = @user.id
        get :edit, id: @klass.id
      end

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it 'renders edit template' do
        expect(response).to render_template :edit
      end

      it 'assigns correct klass to variables' do
        expect(assigns[:klass]).to eq(@klass)
      end

      it 'decorates klass' do
        expect(assigns[:klass]).to be_decorated
      end
    end

    context 'while logged in to different user' do
      before :all do
        @show = create(:show)
        @klass = create(:klass, show: @show)
      end

      before :each do
        session[:user_id] = @user.id
        get :edit, id: @klass.id
      end

      it 'redirects to show view' do
        expect(response).to redirect_to show_url(@show)
      end
    end
  end

  describe "POST #update" do
    before :all do
      @user = create(:user)
    end

    context 'while logged in as owner' do
      before :all do
        @show = create(:show, user: @user)
        @klass = create(:klass, show: @show)
      end

      context 'given valid information' do
        before :each do
          session[:user_id] = @user.id
          post :update, {id: @klass.id, klass: attributes_for(:klass, name: "Kitten Class")}
        end

        it 'saves the changes to the database' do
          @klass.reload
          expect(@klass.name).to eq("Kitten Class")
        end

        it 'redirects to show' do
          expect(response).to redirect_to show_url(@show)
        end
      end

      context 'given invalid information' do
        before :each do
          session[:user_id] = @user.id
          post :update, {id: @klass.id, klass: attributes_for(:invalid_klass)}
        end

        it 'does not save the changes' do
          @klass.reload
          expect(@klass.name).to_not be nil
        end

        it 'renders the edit template' do
          expect(response).to render_template :edit
        end
      end
    end

    context 'while logged in to a different user' do
      before :all do
        @show = create(:show)
        @klass = create(:klass, show: @show)
      end

      before :each do
          session[:user_id] = @user.id
          post :update, {id: @klass.id, klass: attributes_for(:klass)}
      end

      it 'redirects to show view' do
        expect(response).to redirect_to show_url(@show)
      end
    end
  end

  describe "DELETE #destroy" do
    before :all do
      @user = create(:user)
      @show = create(:show, user: @user)
    end

    context 'while logged in as the owner' do
      before :each do
        session[:user_id] = @user.id
      end

      context 'given a destructable record' do
        before :each do
          @klass = create(:klass, show: @show)
          delete :destroy, id: @klass.id
        end

        it 'finds the correct klass' do
          expect(assigns[:klass]).to eq(@klass)
        end

        it 'destroys the record' do
          expect(Klass.where(id: @klass.id).size).to be 0
        end

        it 'redirects to show view' do
          expect(response).to redirect_to show_url(@show)
        end
      end

      context 'given an indestructable record' do
        before :each do
          @klass = create(:indestructable_klass, show: @show)
          delete :destroy, id: @klass.id
        end

        it 'finds the correct klass' do
          expect(assigns[:klass]).to eq(@klass)
        end

        it 'does not destroy the klass' do
          expect(Klass.where(id: @klass.id).size).to be 1
        end

        it 'redirects to view class' do
          expect(response).to redirect_to class_url(@klass)
        end
      end
    end

    context 'while logged in as a different user' do
      before :all do
        @klass = create(:klass, show: @show)
      end

      before :each do
        session[:user_id] = create(:user).id
        delete :destroy, id: @klass.id
      end

      it 'does not delete the klass' do
        expect(Klass.where(id: @klass.id).size).to be 1
      end

      it 'redirects to klass view' do
        expect(response).to redirect_to class_url(@klass)
      end
    end
  end

  describe 'POST move_up' do
    before :all do
      @user = create(:user)
      @show = create(:show, user: @user)
      @klass = create(:klass, show: @show, name: "Special Class")
      4.times do
        create(:klass, show: @show)
      end
    end

    context 'while logged in to show owner' do
      before :each do
        session[:user_id] = @user.id
        @klass.move_to_bottom
      end

      it 'lowers position by one' do
        expect{
          post :move_up, id: @klass.id
          @klass.reload
        }.to change(@klass,:position).by -1
      end

      it 'redirects to show view' do
        post :move_up, id: @klass.id
        expect(response).to redirect_to show_url(@klass.show)
      end
    end

    context 'while logged in to a different user' do
      before :each do
        session[:user_id] = create(:user).id
        @klass.move_to_bottom
      end

      it 'leaves position unchangedd' do
        expect{
          post :move_up, id: @klass.id
          @klass.reload
        }.to_not change(@klass,:position)
      end
     
      it 'redirects to show view' do
        post :move_up, id: @klass.id
        expect(response).to redirect_to show_url(@klass.show)
      end
    end
  end


  describe 'POST move_down' do
    before :all do
      @user = create(:user)
      @show = create(:show, user: @user)
      4.times do
        create(:klass, show: @show)
      end
      @klass = create(:klass, show: @show, name: "Special Class")
    end

    context 'while logged in to show owner' do
      before :each do
        session[:user_id] = @user.id
        @klass.move_to_top
      end

      it 'lowers position by one' do
        expect{
          post :move_down, id: @klass.id
          @klass.reload
        }.to change(@klass,:position).by 1
      end

      it 'redirects to show view' do
        post :move_down, id: @klass.id
        expect(response).to redirect_to show_url(@klass.show)
      end
    end

    context 'while logged in to a different user' do
      before :each do
        session[:user_id] = create(:user).id
        @klass.move_to_bottom
      end

      it 'leaves position unchangedd' do
        expect{
          post :move_down, id: @klass.id
          @klass.reload
        }.to_not change(@klass,:position)
      end
     
      it 'redirects to show view' do
        post :move_down, id: @klass.id
        expect(response).to redirect_to show_url(@klass.show)
      end
    end
  end
end
