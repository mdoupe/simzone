require 'rails_helper'

RSpec.describe RequirementsController, type: :controller do
  describe 'GET #new' do
    before :each do
      @user = create(:user)
      @show = create(:show, user: @user)
      @klass = create(:klass, show: @show)
    end

    context 'while logged in to the owner' do
      before :each do
        session[:user_id] = @user.id
      end
      
      context 'of unentered show' do
        before :each do
          get :new, class_id: @klass
        end

        it 'returns http success' do
          expect(response).to have_http_status :success
        end

        it 'it renders the new template' do
          expect(response).to render_template :new
        end

        it 'assigns a new requirement to @requirement' do
          expect(assigns[:requirement]).to be_a_new Requirement
        end

        it 'decorates the requirement' do
          expect(assigns[:requirement]).to be_decorated
        end
      end

      context 'of entered show' do
        before :each do
          @klass.horses << create(:horse)
          get :new, class_id: @klass
        end

        it 'redirects to klass' do
          expect(response).to redirect_to class_url(@klass)
        end
      end
    end

    context 'while logged in to different user' do
      before :each do
        session[:user_id] = create(:user).id
        get :new, class_id: @klass
      end

      it 'redirects to klass view' do
        expect(response).to redirect_to class_url(@klass)
      end
    end
  end

  describe 'POST #create' do
    before :all do
      @user = create(:user)
    end

    before :each do
      session[:user_id] = @user.id
    end

    context 'while logged in as klass owner' do
      before :all do
        @show = create(:show, user: @user)
        @klass = create(:klass, show: @show)
      end

      context 'of unentered show' do
        context 'given valid data' do
          before :all do
            @requirement = attributes_for(:requirement)
          end

          it 'saves requirement to database' do
            expect{
              post :create, {class_id: @klass.id, requirement: @requirement}
            }.to change(Requirement,:count).by 1
          end

          it 'redirects to klass view' do
            post :create, {class_id: @klass.id, requirement: @requirement}
            expect(response).to redirect_to class_url(@klass)
          end
        end

        context 'given invald data' do
          before :all do
            @requirement = attributes_for(:requirement, values: nil)
          end

          it 'does not save the requirement' do
            expect{
              post :create, {class_id: @klass.id, requirement: @requirement}
            }.to_not change(Requirement,:count)
          end

          it 'renders the new view' do
            post :create, {class_id: @klass.id, requirement: @requirement}
            expect(response).to render_template :new
          end
        end
      end

      context 'of entered show' do
        before :each do
          @klass.horses << create(:horse)
        end

        it 'does not create the requirement' do
          expect{
            post :create, {class_id: @klass.id, requirement: attributes_for(:requirement, klass: @klass)}
          }.to_not change(Requirement, :count)
        end

        it 'redirects to klass' do
          post :create, {class_id: @klass.id, requirement: attributes_for(:requirement, klass: @klass)}
          expect(response).to redirect_to class_url(@klass)
        end
      end
    end

    context 'while logged in as different user' do
      before :all do
        @klass = create(:klass)
        @requirement = attributes_for(:requirement)
      end

      before :each do
        post :create, {class_id: @klass.id, requirement: @requirement}
      end

      it 'redirects to klass view' do
        expect(response).to redirect_to class_url(@klass)
      end
    end
  end

  describe 'GET #edit' do
    before :all do
      @user = create(:user)
    end

    before :each do
      session[:user_id] = @user.id
    end

    context 'while logged in as klass owner' do
      before :all do
        @show = create(:show, user:@user)
        @klass = create(:klass, show: @show)
        @req = create(:requirement, klass: @klass)
      end

      context 'of unentered klass' do
        before :each do
          get :edit, id: @req.id
        end

        it 'returns http success' do
          expect(response).to have_http_status :success
        end

        it 'renders the edit template' do
          expect(response).to render_template :edit
        end

        it 'assigns the correct requirement to @requirement' do
          expect(assigns[:requirement]).to eq @req
        end

        it 'decorates @requirement' do
          expect(assigns[:requirement]).to be_decorated
        end
      end

      context 'of entered klass' do
        before :all do
          @klass.horses << create(:horse)
        end

        before :each do
          get :edit, id: @req.id
        end

        it 'redirects to klass view' do
          expect(response).to redirect_to class_url(@klass)
        end
      end
    end

    context 'while logged in as different user' do
      before :all do
        @req = create(:requirement)
      end

      it 'redirects to klass view' do
        get :edit, id: @req.id
        expect(response).to redirect_to class_url(@req.klass.id)
      end
    end
  end

  describe 'POST #update' do
    before :all do
      @user = create(:user)
    end

    context 'while logged in as owner' do
      before :all do
        @show = create(:show, user: @user)
        @klass = create(:klass, show: @show)
        @req = create(:requirement, {klass: @klass, quality: "color", values: "palomino,buckskin"})
      end

      context 'of unentered klass' do
        before :each do
          session[:user_id] = @user.id
        end

        context "given valid info" do
          before :each do
            post :update, {id: @req.id, requirement: attributes_for(:requirement, {klass: @klass, quality: "color", values: "perlino,cremelo"})}
            @req.reload
          end

          it 'saves record' do
            expect(@req.values).to eq "perlino,cremelo"
          end

          it 'redirects to view klass' do
            expect(response).to redirect_to class_url(@klass)
          end
        end

        context 'given invalid info' do
          before :each do
            post :update, {id: @req.id,requirement: attributes_for(:requirement, {klass: @klass, quality: "color", values: nil})}
            @req.reload
          end

          it 'does not save given valid info' do
            expect(@req.values).to eq "palomino,buckskin"
          end

          it 'renders the edit view' do
            expect(response).to render_template :edit
          end
        end
      end

      context 'of entered show' do
        before :all do
          @klass.horses << create(:horse)
        end

        before :each do
          post :update, {id: @req.id, requirement: attributes_for(:requirement, {klass: @klass, quality: "color", values: "perlino,cremelo"})}
          @req.reload
        end

        it 'does not update the requirement' do
          expect(@req.values).to_not eq "perlino,cremelo"
        end
      end
    end

    context 'while logged in to different user' do
      before :all do
        @show = create(:show)
        @klass = create(:klass, show: @show)
        @req = create(:requirement, klass: @klass)
      end

      before :each do
        session[:user_id] = @user.id
      end

      it 'redirects to klass view' do
        post :update, id: @req.id
        expect(response).to redirect_to class_url(@klass)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @req = create(:requirement)
    end

    context 'logged in to owner' do
      before :each do
        session[:user_id] = @req.klass.show.user.id
      end

      context 'given a destructable requirement' do
        it 'deletes the requirement' do
          expect{
          delete :destroy, id: @req.id
          }.to change(Requirement,:count).by -1
        end

        it 'redirects to klass view' do
          delete :destroy, id: @req.id
          expect(response).to redirect_to class_url(@req.klass)
        end
      end

      context 'given an indestructable requirement' do
        before :each do
          @req.klass.horses << create(:horse)
        end

        it 'does not delete the requirement' do
           expect{
            delete :destroy, id: @req.id
           }.to_not change(Requirement,:count)
        end

        it 'redirects to klass view' do
          delete :destroy, id: @req.id
          expect(response).to redirect_to class_url(@req.klass)
        end
      end
    end

    context 'logged in to different user' do
      before :each do
        session[:user_id] = create(:user).id
      end

      it 'does not destroy the record' do
        expect{
        delete :destroy, id: @req.id
        }.to_not change(Requirement,:count)
      end

      it 'redirects to klass view' do
        delete :destroy, id: @req.id
        expect(response).to redirect_to class_url(@req.klass)
      end
    end
  end
end