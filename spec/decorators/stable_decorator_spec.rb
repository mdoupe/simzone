require 'spec_helper'

describe StableDecorator do
    describe '.link' do
    before :each do
      @stable = create(:stable).decorate
    end

    it 'links the stables name to its show page' do
      expect(@stable.link).to have_link(@stable.name, href: stable_url(@stable))
    end
  end
end
