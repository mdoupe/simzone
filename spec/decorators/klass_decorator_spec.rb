require 'spec_helper'

describe KlassDecorator do
  it 'displays spots left' do
    @i = 5
    @klass = create(:klass, entry_limit: @i).decorate
    @i.times do
      expect(@klass.spots).to eq @i
      @klass.horses << create(:horse)
      @i -= 1
    end
  end
end
