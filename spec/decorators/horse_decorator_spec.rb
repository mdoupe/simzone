require 'spec_helper'

describe HorseDecorator do

  describe '.age' do
    before :each do
      @horse = create(:horse).decorate
    end
    it 'returns the age of the horse' do
      expect(@horse.age).to eq(Date.today.strftime("%Y").to_i - @horse.year)
    end
  end

  describe '.info' do
    before :each do
      @horse = create(:horse).decorate
    end

    it 'prints the basic info of the horse' do
      expect(@horse.info).to eq("#{@horse.year} #{@horse.color} #{@horse.breed} #{@horse.gender}")
    end
  end

  describe '.owner' do
    before :each do
      @user = create(:user)
      @horse = create(:horse, user: @user).decorate
    end

    it 'prints the name of the owner' do
      expect(@horse.owner).to eq(@user.name)
    end
  end

  describe '.link' do
    before :each do
      @horse = create(:horse).decorate
    end

    it 'links the horses name to its show page' do
      expect(@horse.link).to have_link(@horse.name, href: horse_url(@horse))
    end
  end
end
