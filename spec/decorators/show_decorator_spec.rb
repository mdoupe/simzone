require 'spec_helper'

describe ShowDecorator do

  describe '.link' do
    before :each do
      @show = create(:show).decorate
    end

    it 'links the shows name to its show page' do
      expect(@show.link).to have_link(@show.name, href: show_url(@show))
    end
  end
  
end
