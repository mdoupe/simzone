require 'spec_helper'

describe RequirementDecorator do
  describe ".brief" do
    it 'prints the requirement in readable form' do
      @req = create(:requirement, {quality: "breed", values: "arabian,half-arabian"}).decorate
      expect(@req.brief).to eq("Restricted to breeds: Arabian, Half-arabian")
    end
  end
end
