require 'spec_helper'

shared_examples_for "feature_bounce_login" do
  context 'while not logged in' do
    before :each do
      page.set_rack_session(user_id: nil)
    end

    it 'tells me to log in' do
      visit url
      expect(page).to have_content("You must sign in before performing that action.")
    end
  end
end

shared_examples_for "controller_bounce_login_get" do
  context 'while not logged in' do
    before :each do
      session[:user_id] = nil
    end
    
    it 'redirects to login page' do
      get action
      expect(response).to redirect_to(new_session_url)
    end
  end
end

shared_examples_for "controller_bounce_login_post" do
  context 'while not logged in' do
    before :each do
      session[:user_id] = nil
    end

    it 'redirects to login page' do
      post action, id: 1
      expect(response).to redirect_to(new_session_url)
    end
  end
end