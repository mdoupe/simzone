require 'spec_helper'

shared_examples_for "ownable" do
  let(:model) { described_class }
  
  describe 'owned_by?' do
    before :each do
      @object = create(model.to_s.underscore.to_sym)
    end
    context "when given owner" do
      it 'returns true' do
        expect(@object.owned_by?(@object.user)).to be true
      end
    end

    context 'when given other user' do
      it 'returns false' do
        @user = create(:user)
        expect(@object.owned_by?(@user)).to be false
      end
    end
  end

  describe 'transfer' do
    context 'when provided existing user' do
      before :each do
        @object = create(model.to_s.underscore.to_sym)
        @new_owner = create(:user)
      end

      it 'updates user id' do
        @object.transfer(@new_owner)
        expect(@object.user_id).to equal(@new_owner.id)
      end

      it 'returns true' do
        expect(@object.transfer(@new_owner)).to be true
      end
    end

    context 'when provided non-existant user' do
      it 'does not update user id' do
        @object = create(model.to_s.underscore.to_sym)
        @old_owner = @object.user
        @new_owner = build(:user)
        @object.transfer(@new_owner)
        expect(@object.user_id).to equal(@old_owner.id)
      end
    end

    context 'when provided modified/invalid user' do
      it 'does not update user id' do
        @object = create(:horse)
        @old_owner = @object.user
        @new_owner = create(:user)
        @new_owner.name = "no"
        @object.transfer(@new_owner)
        expect(@object.user_id).to equal(@old_owner.id)
      end
    end

    it 'does not save the record' do
      @object = create(model.to_s.underscore.to_sym)
      @new_owner = create(:user)
      @object.transfer(@new_owner)
      @object.reload
      expect(@object.user_id).to_not equal(@new_owner.id)
    end
  end

  describe 'transfer!' do
    before :each do
      @object = create(model.to_s.underscore.to_sym)
      @new_owner = create(:user)
      @object.transfer!(@new_owner)
    end 

    it 'calls the transfer method' do
      expect(@object.user_id).to equal(@new_owner.id)
    end

    it 'saves the record' do
      @object.reload
      expect(@object.user_id).to equal(@new_owner.id)
    end
  end
end