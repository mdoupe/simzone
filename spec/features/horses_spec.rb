require 'rails_helper'

RSpec.feature "Horses", type: :feature do

  describe 'the creation process' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        @stable = create(:stable, user: @user)
        page.set_rack_session(user_id: @user.id)
        @horse = attributes_for(:horse)
      end

      it 'creates my horse' do
        visit new_horse_url
        fill_in "horse_name", with: @horse[:name]
        fill_in "horse_year", with: @horse[:year]
        fill_in "horse_color", with: @horse[:color]
        fill_in "horse_breed", with: @horse[:breed]
        select @horse[:gender], from: "horse_gender"
        select @stable[:name], from: "horse_stable_id"
        click_button "Create Horse"
        expect(page).to have_content("Horse successfully created.")
      end
    end

    include_examples "feature_bounce_login" do let(:url) { new_horse_url } end
  end

  describe 'the update process' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        page.set_rack_session(user_id: @user.id)
      end

      context 'to the records owner' do
        before :each do
          @horse = create(:horse, user: @user)
        end

        it 'updates my horse' do
          visit edit_horse_url(@horse)
          fill_in "horse_name", with: "Captain Pony"
          click_button "Update Horse"
          expect(page).to have_content "Successfully updated Captain Pony."
        end
      end

      context 'to a different user' do
        before :each do
          @horse = create(:horse)
        end

        it 'directs me to view page' do
          visit edit_horse_url(@horse)
          expect(page).to have_content "Only the owner may edit a horse."
        end
      end
    end

    include_examples "feature_bounce_login" do let(:url) { edit_horse_url(create(:horse)) } end
  end

  describe 'the show page' do
    before :each do
      @horse = create(:horse)
    end

    it 'displays the horse\'s information' do
      visit horse_url(@horse)
      expect(page).to have_content "#{@horse.name}"
    end
  end

  describe 'the index page' do
    before :each do
      @a = create(:horse, name: "HorseA")
      @b = create(:horse, name: "HorseB")
      @c = create(:horse, name: "HorseC")
      @names = [@a.name, @b.name, @c.name]
    end

    it 'displays the existing horses' do
      visit horses_url
      @names.each do |name|
        expect(page).to have_content name
      end
    end

    it 'links to the horses pages' do
      visit horses_url
      click_link "#{@names.first}"
      expect(page).to have_content @names.first
    end
  end

  describe 'the my horses page' do
    context 'while logged in' do
      
      before :each do
        @others = Array.new
        3.times do
          @others << create(:bert_horse)
        end
        @me = create(:user)
        @mine = Array.new
        3.times do
          @mine << create(:ernie_horse, user: @me)
        end
        @all = @others + @mine
        page.set_rack_session(user_id: @me.id)
        visit  my_horses_url
      end

      it 'displays all horses that belong to me' do
        @mine.each do |h|
          expect(page).to have_content(h.name)
        end
      end

      it 'displays no horses that do not belong to me' do
        @others.each do |h|
          expect(page).to_not have_content(h.name)
        end
      end
    end

    include_examples "feature_bounce_login" do let(:url) { my_horses_url } end
  end

  describe 'the transfer process' do
    before :each do
      @owner = create(:user)
      @horse = create(:horse, user: @owner)
      @new_owner = create(:user)
    end

    context "while logged in" do
      context 'as the record owner' do
        before :each do
          page.set_rack_session(user_id: @owner.id)
        end
        
        it 'lets me select an owner to transfer my horse to' do
          visit transfer_horses_url(@horse)
          select @new_owner.name, from: "horse_user_id"
          click_button "Transfer Horse"
          expect(page).to have_content "Successfully transferred #{@horse.name} to #{@new_owner.name}."
        end
      end

      context 'as a different user' do
        before :each do
          @logged = create(:user)
          page.set_rack_session(user_id: @logged.id)
        end

        it 'returns me to the view page' do
          visit transfer_horses_url(@horse)
          expect(page).to have_content "Only the owner may transfer a horse."
        end
      end
    end

    include_examples "feature_bounce_login" do let(:url) { transfer_horses_url(create(:horse)) } end
  end

  describe 'the stabling process' do
    context 'while logged in' do
      before :each do
        @logged = create(:user)
        page.set_rack_session(user_id: @logged.id)
      end

      context 'to the horse owner' do
        before :each do
          @horse = create(:horse, user: @logged)
          @stable = create(:stable, user: @logged)
        end

        it 'allows me to stable my horse' do
          visit stable_horses_url(@horse)
          select @stable.name, from: "horse_stable_id"
          click_button "Restable Horse"
          expect(page).to have_content "Successfully stabled #{@horse.name} at #{@stable.name}."
        end
      end

      context 'to a different user' do
        before :each do
          @horse = create(:horse, user: create(:user))
        end

        it 'returns me to the view page' do
          visit stable_horses_url(@horse)
          expect(page).to have_content "Only the owner may restable a horse."
        end
      end
    end

    include_examples "feature_bounce_login" do let(:url) { stable_horses_url(create(:horse)) } end

  end
end