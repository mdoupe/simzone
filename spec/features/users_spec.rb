require 'rails_helper'

RSpec.feature "Users", type: :feature do

  describe 'the signup page' do
    before :each do
      @user = attributes_for(:user)
    end

    context "given valid information" do
      it 'signs me up' do
        visit new_user_url
        fill_in "user_name", with: @user[:name]
        fill_in "user_email", with: @user[:email]
        fill_in "user_password", with: @user[:password]
        fill_in "user_password_confirmation", with: @user[:password_confirmation]
        click_button "Sign Up"
        expect(page).to have_content("Successfully signed up!")
      end
    end

    context 'given invalid information' do
      it 'tells me what is wrong' do
        visit new_user_url
        fill_in "user_name", with: "no"
        fill_in "user_email", with: @user[:email]
        fill_in "user_password", with: @user[:password]
        fill_in "user_password_confirmation", with: @user[:password_confirmation]
        click_button "Sign Up"
        expect(page).to have_content "Name is too short"
      end
    end
  end

  describe 'the user modification page' do
    before :each do
      @user = create(:user)
      page.set_rack_session(user_id: @user.id)
    end

    context 'given valid information' do
      it 'updates my information' do
        visit edit_user_url(@user.id)
        fill_in "user_name", with: "newname"
        fill_in "user_email", with: @user.email
        fill_in "user_password", with: @user.password
        fill_in "user_password_confirmation", with: @user.password_confirmation
        click_button "Update"
        expect(page).to have_content "Successfully updated user information!"
      end
    end

    context 'given invalid information' do
      it 'tells me what is wrong' do
        visit edit_user_url(@user.id)
        fill_in "user_name", with: "no"
        fill_in "user_email", with: @user.email
        fill_in "user_password", with: @user.password
        fill_in "user_password_confirmation", with: @user.password_confirmation
        click_button "Update"
        expect(page).to have_content "Name is too short"
      end
    end

  end

end
