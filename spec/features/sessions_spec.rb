require 'rails_helper'

RSpec.feature "Sessions", type: :feature do

  describe 'the login page' do
    before :each do
      @user = create(:user)
    end

    context 'given correct authentication' do
      
      it 'logs me in' do
        visit new_session_url
        fill_in "name", with: @user.name
        fill_in "password", with: @user.password
        click_button "Sign In"
        expect(page).to have_content("Successfully signed in! Welcome back, #{@user.name}.")
      end

    end

    context 'given incorrect authentication' do
      it 'informs me of an error' do
        visit new_session_url
        fill_in "name", with: @user.name
        fill_in "password", with: "incorrect"
        click_button "Sign In"
        expect(page).to have_content("Unable to sign in. Username or password is incorrect.")
      end
    end
  end

  # describe 'logout' do
  #   before :each do
  #     @user = create(:user)
  #     page.set_rack_session(user_id: @user.id)
  #   end

  #   it 'deletes my session' do
  #     visit u
  #     expect(current_user).to be_nil
  #   end

  # end

end