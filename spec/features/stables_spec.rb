require 'rails_helper'

RSpec.feature "Stables", type: :feature do

  describe 'stable creation' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        page.set_rack_session(user_id: @user.id)
        @stable = attributes_for(:stable)
      end

      it 'lets me create a stable' do
        visit new_stable_url
        fill_in "stable_name", with: @stable[:name]
        fill_in "stable_description", with: @stable[:stable_description]
        click_button "Create Stable"
        expect(page).to have_content "Successfully created #{@stable[:name]}."
      end
    end

    include_examples "feature_bounce_login" do let(:url) { new_stable_url } end
  
  end

  describe 'edit stable' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        page.set_rack_session(user_id: @user.id)
      end

      context 'to the stable owner' do
        before :each do
          @stable = create(:stable, user: @user)
        end

        it 'updates my stable' do
          visit edit_stable_url(@stable)
          fill_in "stable_name", with: "New Stable Name"
          fill_in "stable_description", with: @stable.description
          click_button "Update Stable"
          expect(page).to have_content "Successfully updated New Stable Name."
        end
      end

      context 'to the wrong user' do
        before :each do
          @stable = create(:stable)
        end

        it 'directs me to the view page' do
          visit edit_stable_url(@stable)
          expect(page).to have_content "Only the owner may edit a stable."
        end
      end

      include_examples "feature_bounce_login" do let(:url) { edit_stable_url(create(:stable)) } end
    end  
  end

  describe 'the index page' do
    context 'while logged in' do
      before :each do
        @user = create(:user)
        page.set_rack_session(user_id: @user.id)
        @names = %w(Farm Stable Barl)
        @names.each do |name|
          create(:stable, name: name)
        end
      end

      it 'lists all the stables' do
        visit stables_url
        @names.each do |name|
          expect(page).to have_content(name)
        end
      end

      it 'links to the stables' do
        @stable = Stable.last
        visit stables_url
        click_link @stable.name
        expect(page).to have_content(@stable.description)
      end
    end
  end

  describe 'the show page' do
    before :each do
      @stable = create(:stable)
    end

    it 'displays the stable\'s information' do
      visit stable_url(@stable)
      expect(page).to have_content "#{@stable.name}"
    end
  end

  describe 'the my stables page' do
    context 'while logged in' do
      
      before :each do
        @others = Array.new
        3.times do
          @others << create(:stable)
        end
        @me = create(:user)
        @mine = Array.new
        3.times do
          @mine << create(:alt_stable, user: @me)
        end
        @all = @others + @mine
        page.set_rack_session(user_id: @me.id)
        visit  my_stables_url
      end

      it 'displays all stables that belong to me' do
        @mine.each do |h|
          expect(page).to have_content(h.name)
        end
      end

      it 'displays no stables that do not belong to me' do
        @others.each do |h|
          expect(page).to_not have_content(h.name)
        end
      end
    end

    include_examples "feature_bounce_login" do let(:url) { my_stables_url } end
  end

end
