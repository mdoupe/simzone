require 'rails_helper'

RSpec.feature "Shows", type: :feature do
  describe 'show creation' do
    context 'while logged in' do
      before :all do
        @user = create(:user)
        @stable = create(:stable, user: @user)
        @show = attributes_for(:show)
      end

      before :each do
        page.set_rack_session(user_id: @user.id)
      end

      it 'lets me create a show' do
        visit new_show_path
        fill_in "show_name", with: @show[:name]
        fill_in "show_description", with: @show[:description]
        select @stable.name, from: "show_stable_id"
        select Time.now.year, from: "show_held_at_1i"
        select 14.days.from_now.strftime("%B"), from: "show_held_at_2i"
        select 14.days.from_now.day, from: "show_held_at_3i"
        select "08", from: "show_held_at_4i"
        select "00", from: "show_held_at_5i"
        select "12 Hours", from: "show_deadline"
        select "Standard", from: "show_generator"
        click_button "Create Show"
        expect(page).to have_content "Successfully created #{@show[:name]}."
      end
    end

    include_examples "feature_bounce_login" do let(:url) { new_show_path } end
  end

  describe 'show editing' do
    before :all do
      @user = create(:user)
      @stable = create(:stable, user: @user)
      @show = create(:show, {user: @user, stable: @stable})
    end

    context 'while logged in as owner' do
      before :each do
        page.set_rack_session(user_id: @user.id)
      end

      it 'updates my show' do
        visit show_path(@show)
        click_link "Edit Show"
        fill_in "show_name", with: "Totally New Show Name"
        click_button "Update Show"
        expect(page).to have_content "Successfully updated #{@show.reload.name}."
      end
    end

    context 'while logged in as different user' do
      before :each do
        page.set_rack_session(user_id: create(:user).id)
      end

      it 'directs me to view page' do
        visit edit_show_path(@show)
        expect(page).to have_content "Only the owner may edit a show."
      end
    end
  
    include_examples "feature_bounce_login" do let(:url) { edit_show_path(@show) } end
  end

  describe 'show list' do
    before :each do
      @user = create(:user)
      page.set_rack_session(user_id: @user.id)
    end

    describe 'index' do
      before :all do
        5.times do
          create(:show, name: "This is a Show")
        end
      end

      before :each do
        visit shows_path
      end

      it 'displays all shows' do
        expect(page).to have_content "This is a Show", count: 5
      end
    

      include_examples "feature_bounce_login" do let(:url) { shows_path } end
    end

    describe 'active' do
      before :all do
        3.times do
          create(:show, name: "Active Show")
        end

        3.times do
          create(:inactive_show, name: "Inactive Show")
        end
      end

      before :each do
        visit active_shows_path
      end

      it 'displays only active shows' do
        expect(page).to have_content "Active Show", count: 3
        expect(page).to_not have_content "Inactive Show"
      end

      include_examples "feature_bounce_login" do let(:url) { active_shows_path } end
    end

    describe 'mine' do
      before :each do
        @user = create(:user)

        3.times do
          create(:show, user: @user, name: "My Show")
        end

        3.times do
          create(:alt_show, name: "Not My Show")
        end
        
        page.set_rack_session(user_id: @user.id)

        visit my_shows_path
      end

      it 'shows all of my shows' do
        expect(page).to have_content "My Show", count: 3
      end

      it 'shows no pages that are not mine' do
        expect(page).to_not have_content "Not My Show"
      end
    end
  end

  describe 'show display' do
    before :all do
      @show = create(:show)
    end

    context 'while logged in' do
      before :all do
        3.times do
          @show.klasses << create(:klass)
        end
        @user = User.all.count > 0 ? User.first : create(:user)
      end

      before :each do
        page.set_rack_session(user_id: @user.id)
        visit show_path(@show)
      end

      it 'displays the show' do
        expect(page).to have_content(@show.name)
      end

      it 'lists the classes' do
        expect(page).to have_content(@show.klasses.first.name, count: 3)
      end

      context 'as owner' do
        before :each do
          page.set_rack_session(user_id: @show.user_id)
          visit show_path(@show)
        end
        
        it 'displays control buttons' do
          expect(page).to have_selector("a", text: "Edit Show")
          expect(page).to have_selector("a", text: "Delete Show")
          expect(page).to have_selector("a", text: "Add Class")
        end

        it 'allows ordering of show' do
          expect(page).to have_selector("a",{text: "Move Up", count: 3})
          expect(page).to have_selector("a",{text: "Move Down", count: 3})
        end
      end
    end

    include_examples "feature_bounce_login" do let(:url) { show_path(@show) } end

  end

  describe 'show deletion' do
    before :each do
      @show = create(:show)
    end

    context 'while logged in as show owner' do
      before :each do
        page.set_rack_session(user_id: @show.user.id)
      end

      it 'deletes the show' do
        visit show_path(@show)
        click_link "Delete Show"
        expect(page).to have_content "Successfully deleted show."
      end
    end
  end

  describe 'klass' do
    before :all do
      @show = create(:show)
      @klass = @show.klasses.count > 0 ? @show.klasses.first : create(:klass, show: @show)
      unless @klass.requirements.count == 2
        create(:requirement,{klass: @klass, quality: "breed", values: "arabian"})
        create(:requirement,{klass: @klass, quality: "year", values: "2013"})
      end
      unless @klass.horses.count > 0
        @horse = create(:horse,{breed: "Arabian",year: 2013})
        @klass.horses << @horse
      end
    end

    before :each do
      page.set_rack_session(user_id: @show.user_id)
    end
    
    describe 'addition' do
      it 'adds a klass to show' do
        @klass = attributes_for(:klass)
        visit show_url(@show)
        click_link "Add Class"
        fill_in "klass_name", with: @klass[:name]
        fill_in "klass_discipline", with: @klass[:discipline]
        fill_in "klass_level", with: @klass[:level]
        fill_in "klass_entry_limit", with: @klass[:entry_limit]
        click_button "Create Class"
        expect(page).to have_content "Successfully created #{@klass[:name]}."
      end
    end

    describe 'editing' do
      it 'edits a klass' do
        @klass = @show.klasses.count > 0 ? @show.klasses.first : create(:klass, show: @show)
        visit show_url(@show)
        click_link("Edit Class", href: edit_class_url(@klass))
        fill_in "klass_name", with: "New Class Name"
        click_button "Update Class"
        expect(page).to have_content "Successfully updated New Class Name."
      end
    end


    describe 'view' do
      before :each do
        visit show_url(@show)
        click_link("#{@klass.name}", href: class_url(@klass))
      end

      it 'displays the klass' do
        expect(page).to have_content(@klass.name)
      end

      it 'lists requirements' do
        expect(page).to have_content("Restricted to breeds: Arabian")
        expect(page).to have_content("Restricted to years: 2013")
      end

      it 'lists entries' do
        expect(page).to have_content(@horse.name)
      end
    end
    
    describe 'motion' do
      before :each do
        visit show_url(@show)
      end

      describe 'upward' do
        before :each do
          click_link "Move Up", href: move_up_class_url(@klass)
        end

        it 'changes the order of the klasses' do
          expect(page).to have_content("Successfully reordered classes.")
        end
      end

      describe 'downward' do
        before :each do
          click_link "Move Down", href: move_down_class_url(@klass)
        end

        it 'changes the order of the klasses' do
          expect(page).to have_content("Successfully reordered classes.")
        end
      end
    end

    describe 'deletion ' do
      before :each do
        @klass = create(:klass, show: @show)
        visit show_url(@show)
        click_link "Delete Class", href: class_url(@klass)
      end

      it 'destroys the klass' do
        expect(page).to have_content "Successfully deleted class."
      end
    end

    context 'requirement' do
      before :all do
        @klass = create(:klass, show: @show)
      end

      describe 'creation' do
        before :each do
          visit class_url(@klass)
        end
   
       it 'creates a requirement' do
          click_link "Add Requirement"
          fill_form :requirement, attributes_for(:requirement)
          click_button "Create Requirement"
          expect(page).to have_content "Successfully created requirement."
        end
      end

      describe 'editing' do
        before :each do
          @req = create(:requirement, klass: @klass)
          visit class_url(@klass)
        end

        it 'allows update of requirement' do
          click_link "Edit", href: edit_requirement_path(@req)
          fill_form :requirement, attributes_for(:requirement, values: "palomino,buckskin")
          click_button "Update Requirement"
          expect(page).to have_content "Successfully updated requirement."
        end
      end
        
      describe 'deletion' do
        before :each do
          @req = create(:requirement, klass: @klass)
          visit class_url(@klass)
        end

        it 'allows deletion of requirement' do
          click_link "Delete", href: requirement_path(@req)
          expect(page).to have_content "Successfully deleted requirement."
        end
      end
    end
  end
end
