require 'rails_helper'

RSpec.feature "Entries", type: :feature do
  describe 'creation' do
    before :all do
      @klass = create(:klass, name: "This Class")
      @user = create(:user)
      create(:requirement, {klass: @klass, quality: "breed", values: "arabian"})
    end

    before :each do
      page.set_rack_session(user_id: @user.id)
    end

    it 'allows me to enter a valid horse' do
      @horse = create(:horse, {user: @user, name: "This Horse", breed: "Arabian"})
      visit new_class_entry_url(class_id: @klass.id)
      select @horse.name, from: "entry_horse_id"
      click_button "Enter Horse"
      expect(page).to have_content "Successfully entered This Horse in This Class."
    end

    it 'does not allow me to enter an invalid horse' do
      @horse = create(:horse, {user: @user, breed: "Clydesdale", name: "This Horse"})
      visit new_class_entry_url(class_id: @klass.id)
      select @horse.name, from: "entry_horse_id"
      click_button "Enter Horse"
      expect(page).to have_content "This Horse is not eligible for This Class."
    end
  end

  describe 'deletion' do
    before :all do
      @klass = create(:klass, name: "This Class")
      @user ||= create(:user)
      create(:requirement, {klass: @klass, quality: "breed", values: "arabian"})
      @horse ||= create(:horse, {user: @user, name: "This Horse", breed: "Arabian"})
      @entry = create(:entry, {horse: @horse, klass: @klass})
    end

    before :each do
      page.set_rack_session(user_id: @user.id)
    end

    it 'deletes horse from an open class' do
      visit class_url(@klass)
      click_link "Remove Entry", href: entry_path(@entry)
      expect(page).to have_content "Removed This Horse from This Class."
    end
  end
end
