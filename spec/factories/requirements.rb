FactoryGirl.define do
  factory :requirement do
    association :klass
    quality "color"
    inclusive true
    values "bay,chestnut"
  end
end
