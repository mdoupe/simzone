FactoryGirl.define do
  factory :stable do
    association :user
    name "Superstar Farms"
    description "This is my super awesome stable where my super cool horses live!"

    factory :invalid_stable do
      name "No"
    end

    factory :alt_stable do
      name "Alternative Equine"
    end
  end

end
