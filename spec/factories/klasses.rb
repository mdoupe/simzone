FactoryGirl.define do
  factory :klass do
    association :show
    name "Super Elite Class"
    discipline "Dressage"
    level "Introductory"
    entry_limit 20
    position 0
    judged false

    factory :judged_klass do
      judged true

      factory :indestructable_klass do

      end
    end

    factory :invalid_klass do
      name nil
    end
  end

end
