FactoryGirl.define do
  factory :horse do
    association :user
    name "Horsie"
    year 2013
    color "Grey"
    breed "Percheron"
    gender "Gelding"
    association :stable

    factory :invalid_horse do
      name nil
    end

    factory :bert_horse do
      name "Bert"
      color "Orange"
    end

    factory :ernie_horse do
      name "Ernie"
      color "Yellow"
    end
  end

end
