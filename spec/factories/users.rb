FactoryGirl.define do
  factory :user do
    sequence :name do | n |
       "user#{n}"
     end
    email { "#{name}@example.com" }
    password { "password#{name}" }
    password_confirmation { "#{password}" }

    factory :invalid_user do
      name "no"
    end
  end

end
