FactoryGirl.define do
  factory :result do
    association :klass
    association :horse
    sequence :finish do |n|
      n
    end
  end

end
