FactoryGirl.define do
  factory :offsite_result do
    association :horse
    date { Date.today }
    show "Offsite Show"
    klass "Champion Class"
    finish "1st"
    url "http://www.example.com"
  end
end