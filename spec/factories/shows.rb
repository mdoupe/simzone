FactoryGirl.define do
  factory :show do
    association :user
    association :stable
    name "My Super Fantastic Show"
    description "This is a super fantastic show that I am holding!"
    held_at { rand(7..21).days.from_now }
    deadline { [24,12,0].sample.hours.to_i }
    generator "standard"

    factory :invalid_show do
      name "No"
    end

    factory :destructable_show do

    end


    factory :alt_show do
      name "My Other Super Show"
    end

    factory :inactive_show do
      held_at { 1.day.ago}

      factory :indestructable_show do
      end
    end
  end
end
